import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.3.5.RELEASE"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    kotlin("jvm") version "1.3.72"
    kotlin("plugin.spring") version "1.3.72"
}

group = "com.kokkle"
version = "0.0.1"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.postgresql:postgresql:42.2.11")
    implementation("com.zaxxer:HikariCP:3.2.0")
    implementation("org.springframework.boot:spring-boot-starter-data-jdbc")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.cloud:spring-cloud-gcp-starter-bigquery:1.2.5.RELEASE")
    implementation("io.springfox:springfox-boot-starter:3.0.0")
    implementation("io.springfox:springfox-swagger-ui:3.0.0")
    implementation("org.flywaydb:flyway-core:6.3.3")
    implementation("org.apache.hadoop:hadoop-common:3.3.0") {
        exclude("org.slf4j", "slf4j-log4j12")
    }
    implementation("org.apache.hadoop:hadoop-client:3.3.0") {
        exclude("org.slf4j", "slf4j-log4j12")
    }
    implementation("org.apache.parquet:parquet-avro:1.11.1") {
        exclude("org.slf4j", "slf4j-log4j12")
    }
    //set yetus to 0.13.0 instead of 0.11.0 to resolve the following issues:
    //'dependencies.dependency.version' for jdk.tools:jdk.tools:jar is missing. in org.apache.yetus:audience-annotations:0.11.0
    //'dependencies.dependency.systemPath' for jdk.tools:jdk.tools:jar is missing. in org.apache.yetus:audience-annotations:0.11.0
    implementation("org.apache.yetus:audience-annotations:0.13.0")

    implementation("com.github.jsqlparser:jsqlparser:0.9")
    implementation("com.google.cloud:google-cloud-storage:1.113.12")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("io.zonky.test:embedded-database-spring-test:1.6.0")
    testImplementation("io.mockk:mockk:1.10.2")
}

tasks.withType<Test> {
    useJUnitPlatform()
    if (System.getenv("GOOGLE_APPLICATION_CREDENTIALS") == null) {
        environment("GOOGLE_APPLICATION_CREDENTIALS", "./gcs-test-key.json")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
