/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.mapper

import com.kokkle.cloudcache.dto.CacheMetadataDto
import com.kokkle.cloudcache.dto.InsertDeleteCacheMetadataDto
import com.kokkle.cloudcache.dto.UpdateCacheMetadataDto
import org.springframework.stereotype.Component
import java.sql.ResultSet
import java.time.LocalDate

@Component
class CacheMetadataMapper {

    fun toUpdateCacheMetadataDto(newStartDate: LocalDate, newEndDate: LocalDate, cacheMetadataDto: CacheMetadataDto): UpdateCacheMetadataDto {
        return UpdateCacheMetadataDto(cacheMetadataDto.startDate, cacheMetadataDto.endDate, newStartDate, newEndDate, cacheMetadataDto.tableName, cacheMetadataDto.hitCount, cacheMetadataDto.lastHit)
    }

    fun toInsertDeleteCacheMetadataDto(cacheMetadataDto: CacheMetadataDto): InsertDeleteCacheMetadataDto {
        return InsertDeleteCacheMetadataDto(cacheMetadataDto.startDate, cacheMetadataDto.endDate, cacheMetadataDto.tableName)
    }

    fun mapEntity(rs: ResultSet, num: Int): CacheMetadataDto {
        return CacheMetadataDto(
                rs.getTimestamp("updated_at").toLocalDateTime(),
                rs.getTimestamp("inserted_at").toLocalDateTime(),
                rs.getDate("start_date").toLocalDate(),
                rs.getDate("end_date").toLocalDate(),
                rs.getString("table_name"),
                rs.getInt("hit_count"),
                rs.getTimestamp("last_hit")?.toLocalDateTime()

        )
    }

}