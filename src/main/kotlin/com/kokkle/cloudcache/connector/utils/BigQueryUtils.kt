/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.connector.utils

import com.google.cloud.bigquery.Field
import com.google.cloud.bigquery.StandardSQLTypeName

object BigQueryUtils {
    fun convertToField(columnName: String, type: String): Field {
        val bqType = when(type.toLowerCase()) {
            "integer","int" -> StandardSQLTypeName.INT64
            "date" -> StandardSQLTypeName.DATE
            "datetime" -> StandardSQLTypeName.DATETIME
            "boolean","bool" -> StandardSQLTypeName.BOOL
            "array" -> StandardSQLTypeName.ARRAY
            "numeric" -> StandardSQLTypeName.NUMERIC
            "float" -> StandardSQLTypeName.FLOAT64
            else -> StandardSQLTypeName.STRING
        }
        return Field.of(columnName, bqType)
    }

}
