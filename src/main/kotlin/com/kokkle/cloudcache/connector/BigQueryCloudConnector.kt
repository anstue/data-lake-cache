/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.connector

import com.google.cloud.RetryOption
import com.google.cloud.bigquery.*
import com.google.cloud.bigquery.JobStatistics.LoadStatistics
import com.kokkle.cloudcache.dto.InMemoryTableResultDto
import com.kokkle.cloudcache.dto.QueryMetadataDto
import com.kokkle.cloudcache.dto.RowDto
import com.kokkle.cloudcache.exception.CacheBQException
import com.kokkle.cloudcache.service.FileService
import com.kokkle.cloudcache.service.StorageService
import com.kokkle.cloudcache.util.FileUtils
import com.kokkle.cloudcache.util.MetricNames
import io.micrometer.core.instrument.MeterRegistry
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import org.threeten.bp.Duration
import java.nio.channels.Channels
import java.nio.file.Files
import java.nio.file.Path
import java.time.Instant
import java.util.concurrent.TimeUnit


@Service
@ConditionalOnProperty(name = ["cloud.connector.bigQuery.enabled"])
internal class BigQueryCloudConnector(private val bigquery: BigQuery,
                                      private val meterRegistry: MeterRegistry,
                                      private val storageService: StorageService,
                                      private val fileService: FileService) : CloudDbConnector {

    private val logger = LoggerFactory.getLogger(this::class.java)

    companion object {
       // val DATA_FORMAT = "JSON_NEW_LINE_DELIMITED"
        val DATA_FORMAT = "CSV"
    }

    @Value("\${cloud.connector.bigQuery.location}")
    private lateinit var location: String

    @Value("\${spring.cloud.gcp.bigquery.datasetName}")
    protected lateinit var datasetName: String

    @Value("\${googleStorage.tmpCacheBucketName}")
    private lateinit var tmpCacheBucket: String

    override fun query(query: String, tmpName: String): QueryMetadataDto {
        val tableId = TableId.of(datasetName, tmpName)
        val queryConfig = QueryJobConfiguration.newBuilder(query)
                .setDestinationTable(tableId)
                .build()
        val result = bigquery.query(queryConfig)
        return QueryMetadataDto(result.totalRows, result.schema.fields.map { it.name })

    }

    override fun queryInMemory(query: String): InMemoryTableResultDto {
        val queryConfig = QueryJobConfiguration.newBuilder(query)
                .build()
        val result = bigquery.query(queryConfig)
        //convert to generic structure
        val rows = ArrayList<RowDto>()
        for (row in result.iterateAll()) {
            rows.add(RowDto(convertRowToGenericFormat(row)))
        }
        return InMemoryTableResultDto(result.schema.fields.map { it.name }, rows)
    }

    private fun convertRowToGenericFormat(row: FieldValueList): List<Any> {
        return row.map { cell ->
            if (cell.attribute == FieldValue.Attribute.REPEATED) {
                cell.repeatedValue.map { it.value }
            } else {
                cell.value
            }
        }.toList()
    }

    override fun writeToLocalFile(tmpName: String, pages: Long): List<Path> {

        val startTime = Instant.now().epochSecond
        val tableId = TableId.of(datasetName, tmpName)

        val destinationPath = getTmpGcsFilePath(tmpName, ".csv")

        val extractConfiguration: ExtractJobConfiguration = ExtractJobConfiguration
                .newBuilder(tableId, "gs://$tmpCacheBucket/$destinationPath")
                .setFormat(destinationPath)
                .setFieldDelimiter(";").build()
        val job: Job = bigquery.create(JobInfo.of(extractConfiguration))

        val completedJob = job.waitFor(
                RetryOption.initialRetryDelay(Duration.ofSeconds(1)),
                RetryOption.totalTimeout(Duration.ofMinutes(3)))
        if (completedJob == null) {
            throw CacheBQException("BQ Job for tmp table $tmpName not executed since it no longer exists.")
        } else if (completedJob.status.error != null) {
            throw CacheBQException(
                    """
                BigQuery was unable to extract data for tmp table $tmpName due to an error: 
                ${job.status.error}
                """.trimIndent())
        }
        //val pathJson = FileUtils.getTmpFilePath("download_", ".json")
        val files = storageService.downloadFiles(getTmpGcsFilePathPrefix(tmpName))
        storageService.removeFile(destinationPath) // TODO do this async
        //fileService.convertJsonToCsv(pathJson, pathCsv)
        //TODO doesn't support array/repeated columns

        val totalSeconds = Instant.now().epochSecond - startTime
        logger.info("Wrote bq result to file in $totalSeconds seconds")
        meterRegistry.timer(MetricNames.WRITE_CACHE_TO_TMP_FILE).record(totalSeconds, TimeUnit.SECONDS)
        return files
    }

    private fun getTmpGcsFilePath(tmpName: String, extension: String): String {
        return "${tmpName}_*.$extension"
    }
    private fun getTmpGcsFilePathPrefix(tmpName: String): String {
        return tmpName
    }

    private fun writePageToFile(iterable: Iterable<FieldValueList>, path: Path) {
        var counter = 1L
        val tmpList = mutableListOf<RowDto>()
        for (row in iterable.iterator()) {
            tmpList.add(RowDto(convertRowToGenericFormat(row)))
            if (counter % 1_000_000 == 0L) {
                FileUtils.writeToFile(tmpList, path)
                tmpList.clear()
            }
            counter++
        }
        FileUtils.writeToFile(tmpList, path)
    }

    //naive assumption that data stored in cache has the same size
    override fun queryDryRun(query: String): Long {
        val queryConfig = QueryJobConfiguration.newBuilder(query).setDryRun(true).build()
        val job = bigquery.create(JobInfo.of(queryConfig))
        val statistics: JobStatistics.QueryStatistics = job.getStatistics()
        return statistics.totalBytesProcessed

    }

    override fun loadDataIntoTable(datasetName: String, tableName: String, filePath: Path) {

        val tableId = TableId.of(datasetName, tableName)

        val writeChannelConfiguration = WriteChannelConfiguration.newBuilder(tableId)
                .setFormatOptions(FormatOptions.avro())
                .setUseAvroLogicalTypes(true).build()
        val jobId = JobId.newBuilder().setLocation(location).build()
        val writer = bigquery.writer(jobId, writeChannelConfiguration)

        Channels.newOutputStream(writer).use { stream -> Files.copy(filePath, stream) }

        var job = writer.job
        job = job.waitFor()
        val stats: LoadStatistics = job.getStatistics()
        if (job.status.error != null) {
            throw CacheBQException(job.status.error!!.message + " " + job.status.error!!.reason)
        } else {
            logger.info("Loaded ${stats.outputRows} rows into BQ")
        }
    }

    override fun removeTmpTable(tableName: String) {
        bigquery.delete(TableId.of(datasetName, tableName))
    }


}