/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.connector

import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.dto.TableResultDto
import com.kokkle.cloudcache.service.CacheService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import java.nio.file.Path
import java.time.Instant
import java.util.*

@Service
@ConditionalOnProperty(name = ["cloud.connector.bigQuery.enabled"])
class BigQueryCloudDbConnectorProxy(private val cloudDbConnector: CloudDbConnector) : CloudDbConnectorProxy {

    private val logger = LoggerFactory.getLogger(BigQueryCloudDbConnectorProxy::class.java)

    @Value("\${cloud.connector.bigQuery.maxPageSizeMB}")
    private var maxPageSizeMB: Long = 0

    override fun query(query: String, tableName: String, maxSizeMB: Long): Optional<TableResultDto> {
        val queryResultSize = cloudDbConnector.queryDryRun(query)
        if (queryResultSize / 1024 / 1024 > maxSizeMB) {
            logger.warn("Query($query) too big for cache with ${queryResultSize / 1024 / 1024} MB ")
            return Optional.empty()
        } else {
            val pages = Math.max(1, queryResultSize / (maxPageSizeMB * 1024 * 1024))
            val tmpName = "tmp_" + tableName + Instant.now().nano
            val metadataResult = cloudDbConnector.query(query, tmpName)
            val tmpFilePaths = cloudDbConnector.writeToLocalFile(tmpName, pages)
            cloudDbConnector.removeTmpTable(tmpName)
            logger.info("Created tmp file for query($query) with ${queryResultSize / 1024 / 1024} MB ")
            return Optional.of(TableResultDto(metadataResult.columns, tmpFilePaths))
        }
    }

    override fun loadDataIntoTable(datasetName: String, tableName: String, filePath: Path) {
        cloudDbConnector.loadDataIntoTable(datasetName, tableName, filePath)
    }
}