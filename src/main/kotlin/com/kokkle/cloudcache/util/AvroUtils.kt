/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.util

import com.kokkle.cloudcache.util.PostgresUtils.NOT_NULL
import org.apache.avro.LogicalTypes
import org.apache.avro.Schema
import org.apache.avro.SchemaBuilder
import org.apache.avro.generic.GenericData
import java.time.temporal.ChronoField

object AvroUtils {

    fun createAvroSchemaFromPostgresSchema(postgresSchema: Map<String, String>): Schema {
        val fields = SchemaBuilder.record("cache")
                .namespace("com.kokkle.cloudcache").fields()
        postgresSchema.forEach { key, value ->
            val nullable = !value.toLowerCase().contains(NOT_NULL)
            when (getDatatype(value).toLowerCase()) {
                "integer", "smallint", "int4", "int" -> {
                    if (nullable) {
                        fields.optionalInt(key)
                    } else {
                        fields.requiredInt(key)
                    }
                }
                "bigint", "int8" -> {
                    if (nullable) {
                        fields.optionalLong(key)
                    } else {
                        fields.requiredLong(key)
                    }
                }
                "date" -> {
                    val dateType = LogicalTypes.date().addToSchema(Schema.create(Schema.Type.INT))
                    fields.name(key)
                            .type(dateType)
                            .noDefault()
                }
                "boolean", "bool" -> {
                    if (nullable) {
                        fields.optionalBoolean(key)
                    } else {
                        fields.requiredBoolean(key)
                    }
                }
                "text[]" -> {
                    val arrayType = SchemaBuilder.array().items().stringType()
                    fields.name(key)
                            .type(arrayType)
                            .noDefault()
                }
                "float" -> {
                    if (nullable) {
                        fields.optionalFloat(key)
                    } else {
                        fields.requiredFloat(key)
                    }
                }
                "int[]", "_int", "_int4" -> {
                    val arrayType = SchemaBuilder.array().items().intType()
                    fields.name(key)
                            .type(arrayType)
                            .noDefault()
                }
                "bigint[]", "_int8", "_int64" -> {
                    val arrayType = SchemaBuilder.array().items().longType()
                    fields.name(key)
                            .type(arrayType)
                            .noDefault()
                }
                else -> {
                    if (nullable) {
                        fields.optionalString(key)
                    } else {
                        fields.requiredString(key)
                    }
                }
            }
        }
        return fields.endRecord()
    }

    private fun getDatatype(value: String): String {
        return value.split(" ")[0]
    }

    fun convertToAvroDate(date: String): Long {
        return DateUtils.parse(date).getLong(ChronoField.EPOCH_DAY)
    }

    fun convertIntArray(list: List<Int>): GenericData.Array<Int> {
        val arrayType = SchemaBuilder.array().items().intType()
        return GenericData.Array(arrayType, list)
    }

    fun convertStringArray(list: List<String>): GenericData.Array<String> {
        val arrayType = SchemaBuilder.array().items().stringType()
        return GenericData.Array(arrayType, list)
    }
}