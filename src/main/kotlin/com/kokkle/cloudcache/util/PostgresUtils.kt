/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.util

import java.sql.ResultSet

object PostgresUtils {

    const val NOT_NULL="not null"

    fun getValue(rs: ResultSet, columnName: String, columnType: String): Any {
        return when(columnType.toLowerCase()) {
            "integer","smallint", "int4" -> rs.getInt(columnName)
            "bigint", "int8" -> rs.getLong(columnName)
            "date" -> rs.getDate(columnName)
            "timestamp" -> rs.getTimestamp(columnName)
            "time" -> rs.getTime(columnName)
            "boolean","bool" -> rs.getBoolean(columnName)
            "array" -> rs.getArray(columnName).array
            "numeric", "decimal" -> rs.getBigDecimal(columnName)
            "float" -> rs.getFloat(columnName)
            "int[]","_int", "_int4" -> rs.getArray(columnName).array
            "bigint[]", "_int8", "_int64" -> rs.getArray(columnName).array
            else -> rs.getString(columnName)
        }
    }

    fun convertToMB(sizeOfCache: Long): Long {
        return sizeOfCache / 1024 / 1024
    }
}