/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.util

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

object DateUtils {

    private const val DATE_FORMAT = "yyyy-MM-dd"

    private val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT)

    fun convertToLocalDate(date: Date): LocalDate {
        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate()
    }

    fun convertToLocalDateTime(date: Date): LocalDateTime {
        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime()
    }

    fun parse(date: String): LocalDate {
        return LocalDate.parse(date, dateFormatter)
    }

    fun format(date: LocalDate): String {
        return dateFormatter.format(date)
    }
}