/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.util

import com.kokkle.cloudcache.dto.RowDto
import java.io.FileWriter
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.time.Instant

object FileUtils {

    private const val TMP_FOLDER = "data-lake-cache-"

    fun writeToFile(rows: List<RowDto>, path: Path) {
        val csvRows = rows.map { row ->
            row.joinToString(";") { cell ->
                if (cell is ArrayList<*>) {
                    cell.joinToString(",", "'{", "}'")
                } else {
                    escapeCell(cell)
                }
            }
        }
        FileWriter(path.toString(), true).use { fileWriter ->
            csvRows.forEach { line ->
                fileWriter.write(line + '\n')
            }
        }
    }

    private fun escapeCell(cell: Any) = "'" + cell.toString().replace("'", "''") + "'"

    fun getTmpFilePath(filePrefix: String, fileEnding: String): Path {
        val tempDir = Files.createTempDirectory(TMP_FOLDER)
        return Paths.get(tempDir.toString(), filePrefix + Instant.now().epochSecond + fileEnding)
    }
}