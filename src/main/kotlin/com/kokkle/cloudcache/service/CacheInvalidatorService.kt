/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.dao.CacheDao
import com.kokkle.cloudcache.dao.CacheMetadataDao
import com.kokkle.cloudcache.dto.InsertDeleteCacheMetadataDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Service
class CacheInvalidatorService(private val cacheMetadataDao: CacheMetadataDao,
                              private val cacheDao: CacheDao) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @Transactional
    fun invalidateCache(invalidAfterHours: Int) {
        val oldMetadataList = cacheMetadataDao.getOldMetadata(LocalDateTime.now().plusHours(-invalidAfterHours.toLong()))
        logger.info("Found ${oldMetadataList.size} obsolete cache entries for removal")
        val removalList = mutableListOf<InsertDeleteCacheMetadataDto>()
        oldMetadataList.forEach {
            val dto = InsertDeleteCacheMetadataDto(it.startDate, it.endDate, it.tableName)
            cacheDao.removeData(dto)
            removalList.add(dto)
            logger.info("Removed cache entry for ${it.tableName} between ${it.startDate} and ${it.endDate}")
        }
        if (removalList.isNotEmpty()) {
            cacheMetadataDao.deleteMetadata(removalList)
            logger.info("Removal succeeded")
        }
    }


}
