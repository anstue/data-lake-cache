/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service.postgres

import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.service.SqlParserService
import com.kokkle.cloudcache.util.SqlUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class PostgresSqlParserService(@Value("\${target.partitionColumnName}") private val partitionColumnName: String) : SqlParserService(partitionColumnName) {

    override fun convertQueryForCache(resolvedQueryDto: ResolvedQueryDto): ResolvedQueryDto {
        return resolvedQueryDto.copy(query = resolvedQueryDto.query.replace("$datasetName.", ""))
    }

    override fun getCacheQuery(columns: Set<String>, summableColumnNames: List<String>, resolvedQueryDto: ResolvedQueryDto): String {
        // TODO check order of columns. There can be a mismatch. ATM summable columns need to be put last
        return "SELECT ${columns.filter { !summableColumnNames.contains(it) }.joinToString(",")}" +
                ",${summableColumnNames.joinToString("),sum(", prefix = "sum(", postfix = ")")}" +
                " FROM $datasetName.${resolvedQueryDto.tableName} " +
                "WHERE ${SqlUtils.getOrConditionsForDateRanges(resolvedQueryDto.datePartitions.ranges)} " +
                "GROUP BY ${columns.filter { !summableColumnNames.contains(it) }.joinToString(",")}"
    }
}