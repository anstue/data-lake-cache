/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service

import org.springframework.stereotype.Service
import java.io.BufferedReader
import java.io.FileReader
import java.nio.file.Path
import java.util.*

@Service
class FileService {

    private val batchSize: Int = 10_000

    fun convertJsonToCsv(pathJson: Path, pathCsv: Path) {
        val reader = BufferedReader(FileReader(pathJson.toFile())).use { reader ->
            var ongoing = true;
            while (ongoing) {
                val batch = readBatch(reader, batchSize)
                for (jsonLine in batch) {
                    //val jsonTree: JsonNode = ObjectMapper().readTree(File("some-file"))
                    ///TODO convert to json
                }
                if (batch.size < batchSize) {
                    ongoing = false;
                }
            }
        }

    }

    private fun readBatch(reader: BufferedReader, batchSize: Int): List<String> {
        val result: MutableList<String> = ArrayList()
        for (i in 0 until batchSize) {
            val line: String? = reader.readLine()
            if (line != null) {
                result.add(line)
            } else {
                return result
            }
        }
        return result
    }
}