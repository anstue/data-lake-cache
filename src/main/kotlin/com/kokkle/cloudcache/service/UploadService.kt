/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.connector.CloudDbConnectorProxy
import com.kokkle.cloudcache.dto.UploadDataDto
import com.kokkle.cloudcache.file.CacheAvroWriter
import com.kokkle.cloudcache.util.FileUtils
import org.springframework.stereotype.Service
import java.nio.file.Files
import java.nio.file.Path


@Service
class UploadService(private val cloudDbConnectorProxy: CloudDbConnectorProxy,
                    private val cacheAvroWriter: CacheAvroWriter) {

    fun uploadData(uploadDataDto: UploadDataDto) {
        val parquetPath = FileUtils.getTmpFilePath("upload_", ".avro")
        try {
            cacheAvroWriter.writeToAvro(uploadDataDto.rows, org.apache.hadoop.fs.Path(parquetPath.toUri()))
            cloudDbConnectorProxy.loadDataIntoTable(uploadDataDto.datasetName, uploadDataDto.tableName, parquetPath)
        } finally {
            removeFile(parquetPath)
        }

    }

    private fun removeFile(csvPath: Path) {
        Files.delete(csvPath)
    }

}