/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.dto.ColumnTypeDto
import com.kokkle.cloudcache.dto.DatePartitionDto
import com.kokkle.cloudcache.dto.QueryDto
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.sql.DatePartitionFinder
import net.sf.jsqlparser.parser.CCJSqlParserUtil
import net.sf.jsqlparser.schema.Column
import net.sf.jsqlparser.statement.select.Select
import net.sf.jsqlparser.util.TablesNamesFinder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.annotation.PostConstruct


abstract class SqlParserService(private val partitionColumnName: String) {

    private val logger = LoggerFactory.getLogger(SqlParserService::class.java)

    private lateinit var tableNamePattern: Pattern

    @Value("\${spring.cloud.gcp.bigquery.datasetName}")
    protected lateinit var datasetName: String

    @Value("#{\${target.columns}}")
    private lateinit var columns: Map<String, String>

    @PostConstruct
    fun init() {
        //non-greedy: you can make your dot-star non-greedy, which will make it match as few characters as possible
        tableNamePattern = Pattern.compile(".*FROM $datasetName\\.(.*?) .*", Pattern.CASE_INSENSITIVE)
    }

    fun getPartitions(queryDto: QueryDto): DatePartitionDto {

        val select = CCJSqlParserUtil.parse(queryDto.query) as Select
        val datePartitionDto = DatePartitionFinder(partitionColumnName).findDatePartitions(select)
        if(datePartitionDto.ranges.isEmpty()) {
            throw NotImplementedError("The query is currently not supported. Please provide a date between x and y pattern")
        }
        return datePartitionDto
    }

    fun getTableName(query: String): String {
        try {
            val matcher: Matcher = tableNamePattern.matcher(query)
            if (matcher.matches()) {
                return matcher.group(1)
            }
            logger.debug("Pattern did not match for query $query")
        } catch (e: IllegalStateException) {
            logger.warn("Error on pattern matching for query $query", e)
        }
        throw NotImplementedError("The query is currently not supported. Table name cannot be extracted")
    }

    fun resolveQuery(queryDto: QueryDto): ResolvedQueryDto {
        return ResolvedQueryDto(queryDto.query, queryDto.parameters, getTableName(queryDto.query), getPartitions(queryDto), getColumns(queryDto))
    }

    private fun getColumns(queryDto: QueryDto): List<ColumnTypeDto> {
        val statement = CCJSqlParserUtil.parse(queryDto.query)
        val columns = mutableListOf<Column>()
        val tablesNamesFinder: TablesNamesFinder = object : TablesNamesFinder() {
            override fun visit(tableColumn: Column) {
                columns.add(tableColumn)
            }
        }
        tablesNamesFinder.getTableList(statement as Select)
        return columns.map { ColumnTypeDto(it.columnName, "UNKNOWN") }
    }

    abstract fun convertQueryForCache(resolvedQueryDto: ResolvedQueryDto): ResolvedQueryDto

    // TODO change set columns to list of columns to always have the same sort order
    abstract fun getCacheQuery(columns: Set<String>, summableColumnNames: List<String>, resolvedQueryDto: ResolvedQueryDto): String

}