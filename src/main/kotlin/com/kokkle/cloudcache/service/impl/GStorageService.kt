/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service.impl

import com.google.cloud.ReadChannel
import com.google.cloud.storage.Blob
import com.google.cloud.storage.BlobId
import com.google.cloud.storage.Storage
import com.kokkle.cloudcache.service.StorageService
import com.kokkle.cloudcache.util.FileUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.FileOutputStream
import java.nio.file.Path


@Service
class GStorageService(private val storage: Storage) : StorageService {

    @Value("\${googleStorage.tmpCacheBucketName}")
    private lateinit var tmpCacheBucket: String

    override fun downloadFiles(filePathWithoutWildcard: String): List<Path> {
        val files = mutableListOf<Path>()
        val blobs = storage.list(tmpCacheBucket, Storage.BlobListOption.prefix(filePathWithoutWildcard))
        var counter = 0
        for(blob in blobs.iterateAll()) {
            files.add(downloadFile(blob, FileUtils.getTmpFilePath("download_" + counter, ".csv")))
            counter++
        }
        return files
    }

    private fun downloadFile(blob: Blob, localPath: Path): Path {
        val readChannel: ReadChannel = blob.reader()
        FileOutputStream(localPath.toFile()).use { fileOutputStream ->
            fileOutputStream.channel.transferFrom(readChannel, 0, Long.MAX_VALUE)
            fileOutputStream.close()
        }
        return localPath
    }

    override fun removeFile(destinationPath: String) {
        storage.delete(BlobId.of(tmpCacheBucket, destinationPath))
    }

}