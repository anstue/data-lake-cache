/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service.impl

import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.service.TableCacheQueuer
import org.springframework.stereotype.Service
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.collections.HashSet

@Service
class TableCacheQueuerInMemory : TableCacheQueuer {

    private val queues: MutableMap<String, Queue<ResolvedQueryDto>> = ConcurrentHashMap()
    private val tableLock = HashSet<String>()

    override fun queueCacheRequest(resolvedQueryDto: ResolvedQueryDto) {
        synchronized(this) {
            if (queues.containsKey(resolvedQueryDto.tableName)) {
                queues.get(resolvedQueryDto.tableName)!!.add(resolvedQueryDto)
            } else {
                val queue = LinkedList<ResolvedQueryDto>()
                queue.add(resolvedQueryDto)
                queues.put(resolvedQueryDto.tableName, queue)
            }
        }
    }

    override fun getNextItemFromQueue(): Optional<ResolvedQueryDto> {
        synchronized(this) {
            for (entrySet in queues) {
                if (entrySet.value.size > 0 && !tableLock.contains(entrySet.key)) {
                    tableLock.add(entrySet.key)
                    return Optional.of(entrySet.value.poll())
                }
            }
            return Optional.empty()
        }
    }

    override fun unlockTable(tableName: String) {
        synchronized(this) {
            tableLock.remove(tableName)
        }
    }
}