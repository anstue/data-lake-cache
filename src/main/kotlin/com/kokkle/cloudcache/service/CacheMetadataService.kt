/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.dao.CacheMetadataDao
import com.kokkle.cloudcache.dto.CacheMetadataDto
import com.kokkle.cloudcache.dto.DateRangeDto
import com.kokkle.cloudcache.dto.InsertDeleteCacheMetadataDto
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.mapper.CacheMetadataMapper
import com.kokkle.cloudcache.util.DateUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class CacheMetadataService(private val cacheMetadataDao: CacheMetadataDao,
                           private val cacheMetadataMapper: CacheMetadataMapper) {


    @Value("\${cache.freshAfterHours}")
    private var freshAfterHours: Long = 1

    fun updateMetadata(resolvedQueryDto: ResolvedQueryDto) {
        for (dateRangeDto in resolvedQueryDto.datePartitions.ranges) {
            val startDateString = resolvedQueryDto.datePartitions.ranges[0].fromIncl
            val endDateString = resolvedQueryDto.datePartitions.ranges[0].untilIncl

            val metadataList = cacheMetadataDao.getMetadataBetween(startDateString, endDateString, resolvedQueryDto.tableName)
            val startDate = DateUtils.parse(startDateString)
            val endDate = DateUtils.parse(endDateString)
            // TODO consider previous hit_count and last_hit for new metadata or not?
            if (metadataList.isNotEmpty()) {
                //case 0 metadata inside range, we can remove it
                val metadataToRemove = metadataList.filter { it.startDate >= startDate && it.endDate <= endDate }
                        .map { cacheMetadataMapper.toInsertDeleteCacheMetadataDto(it) }
                cacheMetadataDao.deleteMetadata(metadataToRemove)
                //case 1 start and enddate is outside of range
                val metadataBiggerThanRange = metadataList.filter { it.startDate < startDate && it.endDate > endDate }
                metadataBiggerThanRange.forEach { metadata ->
                    val newFirst = metadata.copy(startDate = endDate.plusDays(1))
                    val newSecond = metadata.copy(endDate = startDate.plusDays(-1))
                    //current metadata is split into two rows
                    cacheMetadataDao.updateMetadata(cacheMetadataMapper.toUpdateCacheMetadataDto(newFirst.startDate, newFirst.endDate, metadata))
                    cacheMetadataDao.insertMetadata(InsertDeleteCacheMetadataDto(newSecond.startDate, newSecond.endDate, metadata.tableName))
                }
                //case 2 start date is inside range
                metadataList.filter { it.startDate >= startDate && it.endDate > endDate }.forEach { original ->
                    val newOne = original.copy(startDate = endDate.plusDays(1))
                    cacheMetadataDao.updateMetadata(cacheMetadataMapper.toUpdateCacheMetadataDto(newOne.startDate, newOne.endDate, original))
                }
                // case 3 end date is inside range
                metadataList.filter { it.startDate < startDate && it.endDate <= endDate }.forEach { original ->
                    val newOne = original.copy(endDate = startDate.plusDays(-1))
                    cacheMetadataDao.updateMetadata(cacheMetadataMapper.toUpdateCacheMetadataDto(newOne.startDate, newOne.endDate, original))
                }
            }
            cacheMetadataDao.insertMetadata(InsertDeleteCacheMetadataDto(startDate, endDate, resolvedQueryDto.tableName))
        }
    }

    fun isAlreadyCached(resolvedQueryDto: ResolvedQueryDto): Boolean {
        // TODO improve to detect partial caches as well
        for (dateRange in resolvedQueryDto.datePartitions.ranges) {
            if (!cacheMetadataDao.isExactlyCached(dateRange.fromIncl, dateRange.untilIncl, resolvedQueryDto.tableName)) {
                return false
            }
        }
        return true
    }

    fun isCached(dateRangeDtos: List<DateRangeDto>, tableName: String): Boolean {
        for (dateRangeDto in dateRangeDtos) {
            if (!(cacheMetadataDao.isExactlyCached(dateRangeDto.fromIncl, dateRangeDto.untilIncl, tableName) ||
                            checkIfCachedInMultipleMetadataEntries(dateRangeDto.fromIncl, dateRangeDto.untilIncl,
                                    cacheMetadataDao.getMetadataBetween(dateRangeDto.fromIncl, dateRangeDto.untilIncl, tableName)))) {
                return false
            }
        }
        return true
    }

    private fun checkIfCachedInMultipleMetadataEntries(startDate: String, endDate: String, metadataBetween: List<CacheMetadataDto>): Boolean {
        if (metadataBetween.size >= 2) {
            val sortedByStartDate = metadataBetween.sortedBy { it.startDate }
            val sortedByEndDate = metadataBetween.sortedByDescending { it.endDate }
            val startOK = sortedByStartDate.first().startDate.toString() <= startDate
            val endOK = sortedByEndDate.first().endDate.toString() >= endDate
            var noBreakInTheMiddle = true
            var lastDate = LocalDate.parse(startDate)
            for (metadata in sortedByStartDate) {
                if (metadata.startDate > lastDate) {
                    noBreakInTheMiddle = false
                    break
                }
                lastDate = metadata.endDate.plusDays(1)
            }
            return noBreakInTheMiddle && startOK && endOK
        } else {
            return false
        }
    }

}