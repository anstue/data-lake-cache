/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.connector.CloudDbConnectorProxy
import com.kokkle.cloudcache.dao.CacheDao
import com.kokkle.cloudcache.dao.InitializerDao
import com.kokkle.cloudcache.dto.CachedTableResultDto
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.util.PostgresUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.nio.file.Files
import java.time.Instant

@Service
class CacheService(private val sqlParserService: SqlParserService,
                   private val cloudDbConnectorProxy: CloudDbConnectorProxy,
                   private val initializerDao: InitializerDao,
                   private val cacheDao: CacheDao,
                   private val cacheMetadataService: CacheMetadataService,
                   private val tableCacheQueuer: TableCacheQueuer) {

    private val logger = LoggerFactory.getLogger(CacheService::class.java)

    @Value("#{\${target.columns}}")
    private lateinit var columns: Map<String, String>

    @Value("\${target.summableColumns}")
    private lateinit var summableColumnNames: List<String>


    @Value("\${cache.maxSizeMB}")
    private var maxSizeMB: Long = 0

    /**
     * Handles a cache request.
     * @return true if query was cached, false if there was not enough space left
     */
    @Transactional
    fun processCacheRequest(resolvedQueryDto: ResolvedQueryDto): Boolean {
        logger.info("Start cache request for table ${resolvedQueryDto.tableName} and dates ${resolvedQueryDto.datePartitions}")
        try {
            val sizeMB = PostgresUtils.convertToMB(cacheDao.getSizeOfCache())
            if (sizeMB >= maxSizeMB) {
                logger.warn("Out of cache space! For query: $resolvedQueryDto")
            } else {
                val startTime = Instant.now().epochSecond
                initializerDao.createTargetTable(resolvedQueryDto.tableName, columns)
                // TODO check for partial cache
                if (!cacheMetadataService.isAlreadyCached(resolvedQueryDto)) {
                    val result = cloudDbConnectorProxy.query(sqlParserService.getCacheQuery(columns.keys, summableColumnNames, resolvedQueryDto), resolvedQueryDto.tableName, maxSizeMB - sizeMB)
                    return if (result.isPresent) {
                        removeObsoleteData(resolvedQueryDto)
                        for (path in result.get().paths) {
                            cacheDao.insertData(path, resolvedQueryDto)
                            cacheMetadataService.updateMetadata(resolvedQueryDto)
                            Files.delete(path)
                        }
                        val durationSeconds = Instant.now().epochSecond - startTime
                        logger.info("Cached result for ${resolvedQueryDto.tableName} and date ranges: ${resolvedQueryDto.datePartitions} in $durationSeconds seconds")
                        true
                    } else {
                        logger.warn("Out of cache space after query result size estimation! For query: $resolvedQueryDto")
                        false
                    }
                } else {
                    logger.info("Results for ${resolvedQueryDto.tableName} and date ranges: ${resolvedQueryDto.datePartitions} already cached")
                }
            }
        } catch (e: Exception) {
            logger.error("Error while caching", e)
            throw e
        }
        return false
    }

    private fun removeObsoleteData(resolvedQueryDto: ResolvedQueryDto) {
        for (dateRange in resolvedQueryDto.datePartitions.ranges) {
            cacheDao.removeData(dateRange.fromIncl, dateRange.untilIncl, resolvedQueryDto.tableName)
        }
    }

    fun getCachedResult(resolvedQueryDto: ResolvedQueryDto): CachedTableResultDto {
        return if (cacheMetadataService
                        .isCached(resolvedQueryDto.datePartitions.ranges, resolvedQueryDto.tableName)) {
            return CachedTableResultDto(true, cacheDao.query(sqlParserService.convertQueryForCache(resolvedQueryDto)))
        } else {
            CachedTableResultDto(false)
        }
    }

    fun queue(resolvedQueryDto: ResolvedQueryDto) {
        tableCacheQueuer.queueCacheRequest(resolvedQueryDto)
    }
}
