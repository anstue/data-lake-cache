/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service

import com.google.common.util.concurrent.ThreadFactoryBuilder
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.atomic.AtomicInteger
import javax.annotation.PostConstruct

@Service
class CacheRequestQueueExecutor(private val executerServiceForCache: ExecutorService,
                                private val tableCacheQueuer: TableCacheQueuer,
                                private val cacheService: CacheService) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    private val activeThreads = AtomicInteger(0)
    private val activeRequests = Collections.synchronizedList(mutableListOf<ResolvedQueryDto>())

    @PostConstruct
    fun init() {
        Executors.newSingleThreadExecutor(
                ThreadFactoryBuilder().setNameFormat("cache-request-queue-executor-%d").build()).submit {
            process()
        }
    }

    fun process() {
        while (!Thread.interrupted()) {
            while (activeThreads.get() <= (executerServiceForCache as ThreadPoolExecutor).maximumPoolSize) {
                val request = tableCacheQueuer.getNextItemFromQueue()
                if (request.isPresent) {
                    val data = request.get()
                    if (isSimilarRequestInProcessing(data)) {
                        logger.info("Cache Request for table ${data.tableName} and dates " +
                                "${data.datePartitions} skipped. Similar request is already in processing.")
                        continue
                    }
                    activeThreads.incrementAndGet()
                    activeRequests.add(data)
                    executerServiceForCache.submit {
                        try {
                            cacheService.processCacheRequest(data)
                        } finally {
                            tableCacheQueuer.unlockTable(data.tableName)
                            activeThreads.decrementAndGet()
                            activeRequests.remove(data)
                        }
                    }
                } else {
                    break
                }
            }
            Thread.sleep(100)
        }
    }

    private fun isSimilarRequestInProcessing(request: ResolvedQueryDto): Boolean {
        activeRequests.forEach {
            if (request.tableName == it.tableName) {
                return true // TODO improve, also check date and adapt it
            }
        }
        return false
    }
}