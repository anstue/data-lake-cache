/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.connector.CloudDbConnector
import com.kokkle.cloudcache.dto.InMemoryTableResultDto
import com.kokkle.cloudcache.dto.QueryDto
import com.kokkle.cloudcache.dto.TableResultDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class QueryProxyService(
        private val sqlParserService: SqlParserService,
        private val cloudDbConnector: CloudDbConnector,
        private val cacheService: CacheService
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    fun proxyQuery(queryDto: QueryDto): InMemoryTableResultDto {
        val resolvedQueryDto = sqlParserService.resolveQuery(queryDto)

        val cachedResultDto = cacheService.getCachedResult(resolvedQueryDto)
        return if (cachedResultDto.cached) {
            logger.info("Cache hit for ${queryDto.query}")
            cachedResultDto.data!!
        } else {
            val tmp = cloudDbConnector.queryInMemory(queryDto.query)
            cacheService.queue(resolvedQueryDto)
            tmp
        }
    }
}