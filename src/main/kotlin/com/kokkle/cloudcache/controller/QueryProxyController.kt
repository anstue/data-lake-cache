/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.controller

import com.kokkle.cloudcache.dto.InMemoryTableResultDto
import com.kokkle.cloudcache.dto.QueryDto
import com.kokkle.cloudcache.dto.TableResultDto
import com.kokkle.cloudcache.service.QueryProxyService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController()
@RequestMapping("/proxy")
class QueryProxyController(private val queryProxyService: QueryProxyService) {

    /**
     *
     */
    @ApiOperation(value = "Endpoint to send sql requests which will be proxied to a cloud db or cache")
    @PostMapping
    fun proxyQuery(
            @ApiParam(value = "Query dto with the sql query")
            @RequestBody queryDto: QueryDto): InMemoryTableResultDto {
        return queryProxyService.proxyQuery(queryDto)
    }
}