/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.controller

import com.kokkle.cloudcache.dto.UploadDataDto
import com.kokkle.cloudcache.service.UploadService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/upload")
class UploadDataController(private val uploadService: UploadService) {

    @ApiOperation(value = "Endpoint for uploading data into the cloud")
    @PutMapping
    fun uploadData(
            @ApiParam(value = "Query dto with the sql query")
            @RequestBody uploadDataDto: UploadDataDto) {
        uploadService.uploadData(uploadDataDto)
    }

}