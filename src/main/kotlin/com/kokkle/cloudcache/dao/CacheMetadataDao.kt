/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.dao

import com.kokkle.cloudcache.dto.CacheMetadataDto
import com.kokkle.cloudcache.dto.InsertDeleteCacheMetadataDto
import com.kokkle.cloudcache.dto.UpdateCacheMetadataDto
import java.time.LocalDateTime


interface CacheMetadataDao {

    fun updateMetadata(cacheMetadataDto: UpdateCacheMetadataDto)
    fun insertMetadata(cacheMetadataDto: InsertDeleteCacheMetadataDto)
    fun deleteMetadata(cacheMetadataDto: List<InsertDeleteCacheMetadataDto>)
    fun getOldMetadata(lastInsertedAt: LocalDateTime): List<CacheMetadataDto>
    fun getMetadataBetween(startDate: String, endDate: String, tableName: String): List<CacheMetadataDto>
    fun isExactlyCached(startDate: String, endDate: String, tableName: String): Boolean
    fun cacheHit(startDate: String, endDate: String, tableName: String)


}