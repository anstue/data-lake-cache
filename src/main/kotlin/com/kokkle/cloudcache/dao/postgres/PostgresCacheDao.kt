/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.dao.postgres

import com.kokkle.cloudcache.dao.CacheDao
import com.kokkle.cloudcache.dto.InMemoryTableResultDto
import com.kokkle.cloudcache.dto.InsertDeleteCacheMetadataDto
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.exception.UnsupportedConnectionException
import com.kokkle.cloudcache.mapper.TargetTableMapper
import com.kokkle.cloudcache.util.DateUtils
import org.postgresql.PGConnection
import org.postgresql.copy.CopyManager
import org.postgresql.core.BaseConnection
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.namedparam.SqlParameterSource
import org.springframework.jdbc.datasource.DataSourceUtils
import org.springframework.stereotype.Repository
import java.io.FileReader
import java.nio.file.Path

@Repository
class PostgresCacheDao(private val namedParameterJdbcTemplate: NamedParameterJdbcTemplate,
                       private val jdbcTemplate: JdbcTemplate, private val targetTableMapper: TargetTableMapper) : CacheDao {

    @Value("\${cache.databaseName}")
    private lateinit var databaseName: String

    @Value("\${target.partitionColumnName}")
    private lateinit var partitionColumnName: String

    companion object {
        private const val SELECT_DB_SIZE = "SELECT pg_database_size(:databaseName)"
    }

    override fun query(queryDto: ResolvedQueryDto): InMemoryTableResultDto {
        val namedParameters: SqlParameterSource = MapSqlParameterSource().addValues(queryDto.parameters)
        return InMemoryTableResultDto(queryDto.columns.map { it.name },
                namedParameterJdbcTemplate.query(queryDto.query, namedParameters, targetTableMapper::mapEntity))
    }

    override fun insertData(result: Path, resolvedQueryDto: ResolvedQueryDto) {
        val connection = DataSourceUtils.getConnection(jdbcTemplate.dataSource!!)
        if (connection.isWrapperFor(PGConnection::class.java)) {
            val copyManager = CopyManager(connection.unwrap(PGConnection::class.java) as BaseConnection)
            FileReader(result.toFile()).use {
                copyManager.copyIn("COPY ${resolvedQueryDto.tableName} FROM STDIN (FORMAT csv, DELIMITER ';', QUOTE '''', HEADER)", it)
            }
        } else {
            throw UnsupportedConnectionException("No postgres connection available!")
        }
    }

    override fun removeData(insertDeleteCacheMetadataDto: InsertDeleteCacheMetadataDto) {
        jdbcTemplate.update("DELETE FROM ${insertDeleteCacheMetadataDto.tableName} " +
                "WHERE $partitionColumnName>='${DateUtils.format(insertDeleteCacheMetadataDto.startDate)}' " +
                "AND $partitionColumnName<='${DateUtils.format(insertDeleteCacheMetadataDto.endDate)}'")
    }

    override fun removeData(fromIncl: String, untilIncl: String, tableName: String) {
        jdbcTemplate.update("DELETE FROM $tableName " +
                "WHERE $partitionColumnName>='$fromIncl' " +
                "AND $partitionColumnName<='$untilIncl'")
    }

    override fun getSizeOfCache(): Long {
        val namedParameters: SqlParameterSource = MapSqlParameterSource().addValue("databaseName", databaseName)
        return namedParameterJdbcTemplate.queryForObject(SELECT_DB_SIZE, namedParameters, Long::class.java)
                ?: Long.MAX_VALUE
    }

}