/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.dao.postgres

import com.kokkle.cloudcache.dao.InitializerDao
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.stereotype.Repository

@Repository
class PostgresInitializerDao(private val jdbcTemplate: JdbcTemplate) : InitializerDao {

    @Value("\${target.partitionColumnName}")
    private lateinit var partitionColumnName: String

    override fun createTargetTable(tableName: String, columns: Map<String, String>) {
        val columnStr = columns.map { entry: Map.Entry<String, String> -> "${entry.key} ${entry.value}" }.joinToString(",")

        jdbcTemplate.update("CREATE TABLE IF NOT EXISTS $tableName ($columnStr);")
        jdbcTemplate.update("CREATE INDEX IF NOT EXISTS ${tableName}_$partitionColumnName ON $tableName ($partitionColumnName);")
    }

}