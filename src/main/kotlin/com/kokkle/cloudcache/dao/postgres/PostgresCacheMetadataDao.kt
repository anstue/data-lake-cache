/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.dao.postgres

import com.kokkle.cloudcache.dao.CacheMetadataDao
import com.kokkle.cloudcache.dto.CacheMetadataDto
import com.kokkle.cloudcache.dto.InsertDeleteCacheMetadataDto
import com.kokkle.cloudcache.dto.UpdateCacheMetadataDto
import com.kokkle.cloudcache.mapper.CacheMetadataMapper
import com.kokkle.cloudcache.util.DateUtils
import org.springframework.jdbc.core.namedparam.*
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.*


@Repository
class PostgresCacheMetadataDao(private val jdbcTemplate: NamedParameterJdbcTemplate,
                               private val cacheMetadataMapper: CacheMetadataMapper) : CacheMetadataDao {

    companion object {
        private const val UPDATE_METADATA = "UPDATE cache_metadata SET hit_count=:hitCount, last_hit=:lastHit, updated_at=NOW()," +
                "start_date=:newStartDate, end_date=:newEndDate " +
                "WHERE start_date=:startDate AND end_date=:endDate AND table_name=:tableName"
        private const val INSERT_METADATA = "INSERT INTO cache_metadata (start_date, end_date, table_name, inserted_at, updated_at, hit_count, last_hit) " +
                "VALUES(:startDate, :endDate, :tableName, NOW(), NOW(), 0, null)"
        private const val DELETE_METADATA = "DELETE FROM cache_metadata WHERE start_date=:startDate AND end_date=:endDate AND table_name=:tableName"
        private const val IS_CACHED = "SELECT 1 FROM cache_metadata WHERE ((start_date<=:startDate AND end_date>=:endDate)) AND table_name=:tableName"
        private const val UPDATE_CACHE_HIT = "UPDATE cache_metadata SET hit_count=hit_count+1, last_hit=NOW() WHERE start_date>=:startDate AND :endDate<=end_date AND table_name=:tableName"
        private const val SELECT_TIME_RANGE = "SELECT * FROM cache_metadata WHERE table_name=:tableName AND (" +
                "(start_date>=:startDate AND end_date<=:endDate) " +  //metadata is within requested time range
                "OR (start_date>=:startDate AND start_date<=:endDate) " + //partial match at the beginning
                "OR (end_date>=:startDate AND end_date<=:endDate) " + //partial match at the end
                "OR (:startDate>=start_date AND :endDate<=end_date)) ORDER BY start_date"  //requested time range is within metadata
        private const val SELECT_OLD_METADATA = "SELECT * FROM cache_metadata WHERE inserted_at<=:before AND updated_at<=:before"
    }

    override fun updateMetadata(cacheMetadataDto: UpdateCacheMetadataDto) {
        val namedParameters: SqlParameterSource = BeanPropertySqlParameterSource(cacheMetadataDto)
        jdbcTemplate.update(UPDATE_METADATA, namedParameters)
    }

    override fun insertMetadata(cacheMetadataDto: InsertDeleteCacheMetadataDto) {
        val namedParameters: SqlParameterSource = MapSqlParameterSource() //
                .addValue("startDate", cacheMetadataDto.startDate)
                .addValue("endDate", cacheMetadataDto.endDate)
                .addValue("tableName", cacheMetadataDto.tableName)
        jdbcTemplate.update(INSERT_METADATA, namedParameters)
    }

    override fun deleteMetadata(cacheMetadataDto: List<InsertDeleteCacheMetadataDto>) {
        val batchValues: MutableList<Map<String, Any>> = ArrayList(cacheMetadataDto.size)
        for (item in cacheMetadataDto) {
            batchValues.add(
                    MapSqlParameterSource("startDate", item.startDate)
                            .addValue("endDate", item.endDate)
                            .addValue("tableName", item.tableName)
                            .values)
        }
        jdbcTemplate.batchUpdate(DELETE_METADATA, batchValues.toTypedArray())
    }

    override fun getOldMetadata(lastInsertedAt: LocalDateTime): List<CacheMetadataDto> {
        val namedParameters: SqlParameterSource = MapSqlParameterSource()
                .addValue("before", lastInsertedAt)
        return jdbcTemplate.query(SELECT_OLD_METADATA, namedParameters, cacheMetadataMapper::mapEntity)
    }

    override fun getMetadataBetween(startDate: String, endDate: String, tableName: String): List<CacheMetadataDto> {
        val namedParameters: SqlParameterSource = MapSqlParameterSource()
                .addValue("startDate", DateUtils.parse(startDate))
                .addValue("endDate", DateUtils.parse(endDate))
                .addValue("tableName", tableName)
        return jdbcTemplate.query(SELECT_TIME_RANGE, namedParameters, cacheMetadataMapper::mapEntity)
    }

    override fun isExactlyCached(startDate: String, endDate: String, tableName: String): Boolean {
        val namedParameters: SqlParameterSource = MapSqlParameterSource()
                .addValue("startDate", DateUtils.parse(startDate))
                .addValue("endDate", DateUtils.parse(endDate))
                .addValue("tableName", tableName)
        return jdbcTemplate.queryForList(IS_CACHED, namedParameters, Boolean::class.java).isNotEmpty()
    }

    override fun cacheHit(startDate: String, endDate: String, tableName: String) {
        val namedParameters: SqlParameterSource = MapSqlParameterSource()
                .addValue("startDate", DateUtils.parse(startDate))
                .addValue("endDate", DateUtils.parse(endDate))
                .addValue("tableName", tableName)
        jdbcTemplate.update(UPDATE_CACHE_HIT, namedParameters)
    }

}