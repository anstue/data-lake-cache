/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.tasks

import com.kokkle.cloudcache.service.CacheInvalidatorService
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class CacheInvalidatorTask(private val cacheInvalidatorService: CacheInvalidatorService) {

    @Value("\${cache.InvalidAfterHours}")
    private var invalidAfterHours: Int = 1

    @Scheduled(fixedDelayString = "\${cache.invalidator.delay}", initialDelayString = "\${cache.invalidator.initialDelay}")
    fun invalidateCache() {
        //TODO check for re-cache if frequently used(hit_count higher than average hit count) and at least 10% space available
        cacheInvalidatorService.invalidateCache(invalidAfterHours)
    }
}