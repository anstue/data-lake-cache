/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.sql

import com.kokkle.cloudcache.dto.DatePartitionDto
import com.kokkle.cloudcache.dto.DateRangeDto
import net.sf.jsqlparser.expression.*
import net.sf.jsqlparser.expression.Function
import net.sf.jsqlparser.expression.operators.arithmetic.*
import net.sf.jsqlparser.expression.operators.conditional.AndExpression
import net.sf.jsqlparser.expression.operators.conditional.OrExpression
import net.sf.jsqlparser.expression.operators.relational.*
import net.sf.jsqlparser.schema.Column
import net.sf.jsqlparser.statement.select.*

open class DatePartitionFinder(private val dateColumnName: String) : SelectVisitor, ExpressionVisitor, ItemsListVisitor, SelectItemVisitor {

    // TODO consider 'not date' expression and throw error!

    private var dateRangeDtos = mutableListOf<DateRangeDto>()
    private var isDateColumn = false
    private var isStartDate = false
    private var expectSingleDate = false
    private var isMinorThanOrEquals = false
    private var isGreaterThanOrEquals = false

    private var lastStartDate: String = ""

    private var lowestLowerLimit = "9999-01-01"
    private var highestUpperLimit = "0000-01-01"
    private var lowerOrUpperLimitSet = false

    private fun init() {
        dateRangeDtos = mutableListOf<DateRangeDto>()
    }

    override fun visit(plainSelect: PlainSelect) {
        if (plainSelect.where != null) {
            plainSelect.where.accept(this)
        }
    }

    override fun visit(setOpList: SetOperationList) {
        throw NotImplementedError("The query is currently not supported. If it contains union all, intersect, etc")
    }

    override fun visit(withItem: WithItem) {
        withItem.selectBody.accept(this)
    }

    fun findDatePartitions(select: Select): DatePartitionDto {
        init()
        if (select.withItemsList != null) {
            for (withItem in select.withItemsList) {
                withItem.accept(this)
            }
        }
        select.selectBody.accept(this)

        if (lowerOrUpperLimitSet) {
            dateRangeDtos.add(DateRangeDto(lowestLowerLimit, highestUpperLimit))
        }

        return DatePartitionDto(dateRangeDtos)
    }

    override fun visit(nullValue: NullValue) {
        // ignore
    }

    override fun visit(function: Function?) {
        // ignore
    }

    override fun visit(signedExpression: SignedExpression?) {
        // ignore
    }

    override fun visit(jdbcParameter: JdbcParameter?) {
        // ignore
    }

    override fun visit(jdbcNamedParameter: JdbcNamedParameter?) {
        // ignore
    }

    override fun visit(doubleValue: DoubleValue?) {
        // ignore
    }

    override fun visit(longValue: LongValue?) {
        // ignore
    }

    override fun visit(dateValue: DateValue) {
        // ignore
    }

    override fun visit(timeValue: TimeValue?) {
        // ignore
    }

    override fun visit(timestampValue: TimestampValue?) {
        // ignore
    }

    override fun visit(parenthesis: Parenthesis) {
        if (!parenthesis.isNot) {
            parenthesis.expression.accept(this)
        }
    }

    override fun visit(stringValue: StringValue) {
        if (isDateColumn) {
            if (expectSingleDate) {
                dateRangeDtos.add(DateRangeDto(stringValue.value, stringValue.value))
            } else if (isGreaterThanOrEquals && stringValue.value < lowestLowerLimit) {
                lowestLowerLimit = stringValue.value
                lowerOrUpperLimitSet = true
            } else if (isMinorThanOrEquals && stringValue.value > highestUpperLimit) {
                highestUpperLimit = stringValue.value
                lowerOrUpperLimitSet = true
            } else if (isStartDate) {
                lastStartDate = stringValue.value
            } else if(lastStartDate.isNotEmpty()){
                dateRangeDtos.add(DateRangeDto(lastStartDate, stringValue.value))
                lastStartDate = ""
            }
        }
    }

    override fun visit(addition: Addition) {
        if (isDateColumn) {
            throw NotImplementedError("The query is currently not supported. Subtraction of partition column not supported")
        }
    }

    override fun visit(division: Division) {
        if (isDateColumn) {
            throw NotImplementedError("The query is currently not supported. Subtraction of partition column not supported")
        }
    }

    override fun visit(multiplication: Multiplication) {
        if (isDateColumn) {
            throw NotImplementedError("The query is currently not supported. Subtraction of partition column not supported")
        }
    }

    override fun visit(subtraction: Subtraction) {
        if (isDateColumn) {
            throw NotImplementedError("The query is currently not supported. Subtraction of partition column not supported")
        }
    }

    override fun visit(andExpression: AndExpression) {
        andExpression.leftExpression.accept(this)
        andExpression.rightExpression.accept(this)
    }

    override fun visit(orExpression: OrExpression) {
        orExpression.leftExpression.accept(this)
        orExpression.rightExpression.accept(this)
    }

    override fun visit(between: Between) {
        between.leftExpression.accept(this)
        if (isDateColumn) {
            isStartDate = true
            between.betweenExpressionStart.accept(this)
            isStartDate = false
            between.betweenExpressionEnd.accept(this)
            isDateColumn = false
        }
    }

    override fun visit(equalsTo: EqualsTo) {
        expectSingleDate = true
        equalsTo.leftExpression.accept(this)
        equalsTo.rightExpression.accept(this)
        expectSingleDate = false
    }

    override fun visit(greaterThan: GreaterThan) {
        greaterThan.leftExpression.accept(this)
        isGreaterThanOrEquals = true
        greaterThan.rightExpression.accept(this)
        isGreaterThanOrEquals = false
    }

    override fun visit(greaterThanEquals: GreaterThanEquals) {
        greaterThanEquals.leftExpression.accept(this)
        isGreaterThanOrEquals = true
        greaterThanEquals.rightExpression.accept(this)
        isGreaterThanOrEquals = false
    }

    override fun visit(inExpression: InExpression) {
        inExpression.leftExpression.accept(this)
        inExpression.rightItemsList.accept(this)
    }

    override fun visit(isNullExpression: IsNullExpression?) {
        // ignore
    }

    override fun visit(likeExpression: LikeExpression?) {
        // ignore
    }

    override fun visit(minorThan: MinorThan) {
        minorThan.leftExpression.accept(this)
        isMinorThanOrEquals = true
        minorThan.rightExpression.accept(this)
        isMinorThanOrEquals = false
    }

    override fun visit(minorThanEquals: MinorThanEquals) {
        minorThanEquals.leftExpression.accept(this)
        isMinorThanOrEquals = true
        minorThanEquals.rightExpression.accept(this)
        isMinorThanOrEquals = false
    }

    override fun visit(notEqualsTo: NotEqualsTo) {
        // ignore
    }

    override fun visit(tableColumn: Column) {
        // TODO also consider tableName for queries with multiple tables
        isDateColumn = tableColumn.columnName == dateColumnName
    }

    override fun visit(subSelect: SubSelect) {
        subSelect.selectBody.accept(this)
    }

    override fun visit(expressionList: ExpressionList) {
        expectSingleDate = true
        expressionList.expressions.forEach { it.accept(this) }
        expectSingleDate = false
    }

    override fun visit(multiExprList: MultiExpressionList?) {
        // ignore
    }

    override fun visit(caseExpression: CaseExpression?) {
        // ignore
    }

    override fun visit(whenClause: WhenClause?) {
        // ignore
    }

    override fun visit(existsExpression: ExistsExpression?) {
        // ignore
    }

    override fun visit(allComparisonExpression: AllComparisonExpression?) {
        // ignore
    }

    override fun visit(anyComparisonExpression: AnyComparisonExpression?) {
        // ignore
    }

    override fun visit(concat: Concat?) {
        // ignore
    }

    override fun visit(matches: Matches?) {
        // ignore
    }

    override fun visit(bitwiseAnd: BitwiseAnd?) {
        // ignore
    }

    override fun visit(bitwiseOr: BitwiseOr?) {
        // ignore
    }

    override fun visit(bitwiseXor: BitwiseXor?) {
        // ignore
    }

    override fun visit(cast: CastExpression?) {
        // ignore
    }

    override fun visit(modulo: Modulo?) {
        // ignore
    }

    override fun visit(aexpr: AnalyticExpression?) {
        // ignore
    }

    override fun visit(eexpr: ExtractExpression?) {
        // ignore TODO implement later
    }

    override fun visit(iexpr: IntervalExpression?) {
        // ignore TODO implement later
    }

    override fun visit(oexpr: OracleHierarchicalExpression?) {
        // ignore
    }

    override fun visit(rexpr: RegExpMatchOperator?) {
        // ignore
    }

    override fun visit(allColumns: AllColumns) {
        allColumns.accept(this)
    }

    override fun visit(allTableColumns: AllTableColumns?) {
        // ignore
    }

    override fun visit(selectExpressionItem: SelectExpressionItem?) {
        // ignore
    }
}