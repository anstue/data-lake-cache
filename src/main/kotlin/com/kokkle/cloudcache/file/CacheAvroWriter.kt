/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.file

import com.kokkle.cloudcache.dto.InMemoryTableResultDto
import com.kokkle.cloudcache.dto.RowDto
import com.kokkle.cloudcache.util.AvroUtils
import org.apache.avro.LogicalTypes
import org.apache.avro.Schema
import org.apache.avro.file.DataFileReader
import org.apache.avro.file.DataFileWriter
import org.apache.avro.generic.GenericData
import org.apache.avro.generic.GenericDatumReader
import org.apache.avro.generic.GenericDatumWriter
import org.apache.avro.generic.GenericRecord
import org.apache.avro.io.DatumReader
import org.apache.avro.io.DatumWriter
import org.apache.hadoop.fs.Path
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.io.File
import java.time.LocalDate


@Component
class CacheAvroWriter {

    @Value("#{\${target.columns}}")
    private lateinit var columns: Map<String, String>

    fun writeToAvro(rowsToWrite: List<RowDto>, path: Path) {

        val cacheSchema = AvroUtils.createAvroSchemaFromPostgresSchema(columns)

        val writer: DatumWriter<GenericRecord> = GenericDatumWriter<GenericRecord>(cacheSchema)
        DataFileWriter<GenericRecord>(writer).use { dataFileWriter ->
            dataFileWriter.create(cacheSchema, File(path.toUri()))
            for (row in rowsToWrite) {
                val record = GenericData.Record(cacheSchema)
                cacheSchema.fields.forEach { field ->
                    if (field.schema().type.getName() == "array") {
                        if (field.schema().elementType.name == "int") {
                            record.put(field.name(), AvroUtils.convertIntArray(row[field.pos()] as List<Int>))
                        } else if (field.schema().elementType.name == "string") {
                            record.put(field.name(), AvroUtils.convertStringArray(row[field.pos()] as List<String>))
                        }
                    } else if (field.schema().logicalType != null
                            && field.schema().logicalType.name == "date") {
                        record.put(field.name(), AvroUtils.convertToAvroDate(row[field.pos()] as String))
                    } else {
                        record.put(field.name(), row[field.pos()])
                    }
                }
                dataFileWriter.append(record)
            }

        }
    }

    fun readFromAvro(path: Path): InMemoryTableResultDto {
        val rows = mutableListOf<RowDto>()
        val columns = mutableListOf<String>()
        val reader: DatumReader<GenericRecord> = GenericDatumReader<GenericRecord>()
        DataFileReader<GenericRecord>(File(path.toUri()), reader).use { fileReader ->
            while (fileReader.hasNext()) {
                val record: GenericRecord = fileReader.next()
                if (columns.isEmpty()) {
                    columns.addAll(record.schema.fields.map { it.name() })
                }
                rows.add(RowDto(record.schema.fields.map { field ->
                    if (LogicalTypes.date() == field.schema().logicalType) {
                        LocalDate.ofEpochDay((record[field.name()] as Int).toLong())
                    } else if (Schema.Type.STRING == field.schema().type &&
                            record[field.name()] != null) {
                        record[field.name()].toString()
                    } else {
                        record[field.name()]
                    }
                }))
            }
        }
        return InMemoryTableResultDto(columns, rows)
    }

}