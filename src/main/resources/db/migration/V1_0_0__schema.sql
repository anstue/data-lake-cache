CREATE TABLE cache_metadata (
 start_date DATE NOT NULL,
 end_date DATE NOT NULL,
 table_name VARCHAR(30),
 inserted_at TIMESTAMP,
 updated_at TIMESTAMP,
 hit_count INT DEFAULT 0,
 last_hit TIMESTAMP,
 PRIMARY KEY (start_date, end_date, table_name)
);

CREATE INDEX cache_metadata_table_name on cache_metadata(table_name);
CREATE INDEX cache_metadata_inserted_at on cache_metadata(inserted_at);
CREATE INDEX cache_metadata_updated_at on cache_metadata(updated_at);