INSERT INTO cache_metadata (start_date, end_date, table_name, inserted_at, updated_at, hit_count) VALUES
('2020-01-01', '2020-02-01', 'cache_test_1', NOW() - interval '59 minute', NOW() - interval '59 minute', 0),
('2020-01-01', '2020-02-01', 'cache_test_2', NOW() - interval '121 minute', NOW() - interval '121 minute', 0),
('2020-02-02', '2020-03-01', 'cache_test_1', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 0),
('2020-03-02', '2020-04-01', 'cache_test_1', NOW(), NOW(), 0);

DROP TABLE IF EXISTS cache_test_1;
DROP TABLE IF EXISTS cache_test_2;

CREATE TABLE cache_test_1 (
 event_type VARCHAR(30),
 date DATE NOT NULL,
 value INT,
 ids INT[]
);

CREATE TABLE cache_test_2 (
 event_type VARCHAR(30),
 date DATE NOT NULL,
 value INT,
 ids INT[]
);
INSERT INTO cache_test_1 (event_type, date, value, ids) VALUES
('T1', '2020-01-01',1, '{1}'),
('T1', '2020-01-15',2, '{1}'),
('T1', '2020-02-01',3, '{1}'),
('T2', '2020-02-02',4, '{1}'),
('T2', '2020-02-15',5, '{1}'),
('T2', '2020-03-01',6, '{1}'),
('T3', '2020-03-15',7, '{1}')
;

INSERT INTO cache_test_2 (event_type, date, value, ids) VALUES
('T1', '2020-01-01',1, '{1}'),
('T1', '2020-01-15',2, '{1}'),
('T1', '2020-02-01',3, '{1}')
;