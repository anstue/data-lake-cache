INSERT INTO cache_metadata (start_date, end_date, table_name, inserted_at, updated_at, hit_count) VALUES
('2020-01-01', '2020-02-01', 'cache_test_1', NOW(), NOW(), 0),
('2020-01-01', '2020-02-01', 'bad_table', NOW(), NOW(), 0),
('2020-02-02', '2020-03-01', 'cache_test_1', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 0),
('2020-03-02', '2020-04-01', 'cache_test_1', NOW(), NOW(), 0),
('2021-01-01', '2021-01-01', 'cache_test_1', NOW(), NOW(), 0),

('2018-01-01', '2018-02-01', 'cache_test_1', NOW(), NOW(), 0),
('2018-01-01', '2018-02-01', 'bad_table', NOW(), NOW(), 0),
('2018-02-03', '2018-03-01', 'cache_test_1', NOW(), NOW(), 0),
('2018-03-02', '2018-04-01', 'cache_test_1', NOW(), NOW(), 0);