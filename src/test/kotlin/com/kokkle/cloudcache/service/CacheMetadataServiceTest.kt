/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.dao.CacheMetadataDao
import com.kokkle.cloudcache.dto.DatePartitionDto
import com.kokkle.cloudcache.dto.DateRangeDto
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.testconfig.JdbcTestConfiguration
import com.kokkle.cloudcache.testconfig.ServiceTestConfiguration
import io.mockk.InternalPlatformDsl.toStr
import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@AutoConfigureEmbeddedDatabase(beanName = "dataSource")
@Import(CacheMetadataService::class, ServiceTestConfiguration::class, JdbcTestConfiguration::class)
@PropertySource(value = ["classpath:application-test.properties"])
internal class CacheMetadataServiceTest {
    
    @Autowired
    private lateinit var cacheMetadataService: CacheMetadataService

    @Autowired
    private lateinit var cacheMetadataDao: CacheMetadataDao

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/dao/test-cache-metadata.sql"])
    fun testIsCached() {
        assertTrue(cacheMetadataService.isCached(listOf(DateRangeDto("2020-01-01", "2020-02-01")), "cache_test_1"))
        assertFalse(cacheMetadataService.isCached(listOf(DateRangeDto("2021-01-01", "2021-01-02")), "cache_test_1")) // partial cache at the beginning
        assertFalse(cacheMetadataService.isCached(listOf(DateRangeDto("2020-12-31", "2021-01-01")), "cache_test_1")) // partial cache at the end
        assertFalse(cacheMetadataService.isCached(listOf(DateRangeDto("2020-12-31", "2021-01-02")), "cache_test_1")) // partial cache in the middle
        assertTrue(cacheMetadataService.isCached(listOf(DateRangeDto("2020-01-01", "2020-04-01")), "cache_test_1")) // spawns over multiple rows
        assertFalse(cacheMetadataService.isCached(listOf(DateRangeDto("2018-01-01", "2018-04-01")), "cache_test_1")) // spawns over multiple rows with hole
        assertTrue(cacheMetadataService.isCached(listOf(DateRangeDto("2020-01-01", "2020-01-05")), "cache_test_1"))
        assertFalse(cacheMetadataService.isCached(listOf(DateRangeDto("2019-12-01", "2020-01-05")), "cache_test_1"))
        assertTrue(cacheMetadataService.isCached(listOf(DateRangeDto("2020-04-01", "2020-04-01")), "cache_test_1"))
        assertFalse(cacheMetadataService.isCached(listOf(DateRangeDto("2020-04-01", "2020-04-02")), "cache_test_1"))
    }

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/test-cache-metadata-scenario-1.sql"])
    fun testUpdateMetadata_Scenario1() {
        // just create new metadata
        cacheMetadataService.updateMetadata(ResolvedQueryDto("select * from cache_test_1 where (date BETWEEN '2021-01-01' AND '2021-01-01')", mapOf(),
                "cache_test_1", DatePartitionDto(listOf(DateRangeDto("2021-01-01","2021-01-01"))), listOf()))

        val metadataList = cacheMetadataDao.getMetadataBetween("2020-01-01", "2022-01-01", "cache_test_1")
        assertEquals(2, metadataList.size)
        assertEquals("2020-01-01", metadataList[0].startDate.toString())
        assertEquals("2020-02-01", metadataList[0].endDate.toString())
        assertEquals("2021-01-01", metadataList[1].startDate.toString())
        assertEquals("2021-01-01", metadataList[1].endDate.toString())
    }

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/test-cache-metadata-scenario-2.sql"])
    fun testUpdateMetadata_Scenario2() {
        // replace metadata at beginning
        cacheMetadataService.updateMetadata(ResolvedQueryDto("select * from cache_test_1 where (date BETWEEN '2021-01-01' AND '2021-01-01')", mapOf(),
                "cache_test_1", DatePartitionDto(listOf(DateRangeDto("2021-01-01","2021-01-01"))), listOf()))

        val metadataList = cacheMetadataDao.getMetadataBetween("2020-01-01", "2022-01-01", "cache_test_1")
        assertEquals(2, metadataList.size)
        assertEquals("2021-01-01", metadataList[0].startDate.toString())
        assertEquals("2021-01-01", metadataList[0].endDate.toString())
        assertEquals("2021-01-02", metadataList[1].startDate.toString())
        assertEquals("2021-02-01", metadataList[1].endDate.toString())
    }

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/test-cache-metadata-scenario-2.sql"])
    fun testUpdateMetadata_Scenario3() {
        // replace metadata in the middle
        cacheMetadataService.updateMetadata(ResolvedQueryDto("select * from cache_test_1 where (date BETWEEN '2021-01-03' AND '2021-01-04')", mapOf(),
                "cache_test_1", DatePartitionDto(listOf(DateRangeDto("2021-01-03","2021-01-04"))), listOf()))

        val metadataList = cacheMetadataDao.getMetadataBetween("2020-01-01", "2022-01-01", "cache_test_1")
        assertEquals(3, metadataList.size)
        assertEquals("2021-01-01", metadataList[0].startDate.toString())
        assertEquals("2021-01-02", metadataList[0].endDate.toString())
        assertEquals("2021-01-03", metadataList[1].startDate.toString())
        assertEquals("2021-01-04", metadataList[1].endDate.toString())
        assertEquals("2021-01-05", metadataList[2].startDate.toString())
        assertEquals("2021-02-01", metadataList[2].endDate.toString())
    }

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/test-cache-metadata-scenario-2.sql"])
    fun testUpdateMetadata_Scenario4() {
        // new cached date range bigger
        cacheMetadataService.updateMetadata(ResolvedQueryDto("select * from cache_test_1 where (date BETWEEN '2020-12-31' AND '2021-02-02')", mapOf(),
                "cache_test_1", DatePartitionDto(listOf(DateRangeDto("2020-12-31","2021-02-02"))), listOf()))

        val metadataList = cacheMetadataDao.getMetadataBetween("2020-01-01", "2022-01-01", "cache_test_1")
        assertEquals(1, metadataList.size)
        assertEquals("2020-12-31", metadataList[0].startDate.toString())
        assertEquals("2021-02-02", metadataList[0].endDate.toString())
    }

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/test-cache-metadata-scenario-5.sql"])
    fun testUpdateMetadata_Scenario5() {
        // overlap with two different entries
        cacheMetadataService.updateMetadata(ResolvedQueryDto("select * from cache_test_1 where (date BETWEEN '2021-02-01' AND '2021-02-03')", mapOf(),
                "cache_test_1", DatePartitionDto(listOf(DateRangeDto("2021-02-01","2021-02-03"))), listOf()))

        val metadataList = cacheMetadataDao.getMetadataBetween("2020-01-01", "2022-01-01", "cache_test_1")
        assertEquals(3, metadataList.size)
        assertEquals("2021-01-01", metadataList[0].startDate.toString())
        assertEquals("2021-01-31", metadataList[0].endDate.toString())
        assertEquals("2021-02-01", metadataList[1].startDate.toString())
        assertEquals("2021-02-03", metadataList[1].endDate.toString())
        assertEquals("2021-02-04", metadataList[2].startDate.toString())
        assertEquals("2021-02-28", metadataList[2].endDate.toString())
    }

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/test-cache-metadata-scenario-6.sql"])
    fun testUpdateMetadata_Scenario6() {
        // overlap with entry which has only one day
        cacheMetadataService.updateMetadata(ResolvedQueryDto("select * from cache_test_1 where (date BETWEEN '2021-02-01' AND '2021-02-03')", mapOf(),
                "cache_test_1", DatePartitionDto(listOf(DateRangeDto("2021-02-01","2021-02-03"))), listOf()))

        val metadataList = cacheMetadataDao.getMetadataBetween("2020-01-01", "2022-01-01", "cache_test_1")
        assertEquals(1, metadataList.size)
        assertEquals("2021-02-01", metadataList[0].startDate.toString())
        assertEquals("2021-02-03", metadataList[0].endDate.toString())
    }
}