package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.connector.CloudDbConnectorProxy
import com.kokkle.cloudcache.dto.ColumnTypeDto
import com.kokkle.cloudcache.dto.DatePartitionDto
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.dto.TableResultDto
import com.kokkle.cloudcache.testconfig.JdbcTestConfiguration
import com.kokkle.cloudcache.testconfig.ServiceTestConfiguration
import io.mockk.every
import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.util.ResourceUtils
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*


@ExtendWith(SpringExtension::class)
@AutoConfigureEmbeddedDatabase(beanName = "dataSource")
@Import(CacheService::class, ServiceTestConfiguration::class, JdbcTestConfiguration::class)
@PropertySource(value = ["classpath:application-test.properties"])
@Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/dao/test-cache-metadata.sql"])
internal class CacheServiceTest {

    @Autowired
    private lateinit var cacheService: CacheService

    @Autowired
    private lateinit var cloudDbConnectorProxy: CloudDbConnectorProxy

    @Test
    fun testProcessCacheRequest() {
        val resolvedQueryDto = ResolvedQueryDto(
                "select * from cache_test_1 where (date BETWEEN '2021-02-01' AND '2021-02-01')",
                mapOf(),
                "cache_test_1",
                DatePartitionDto.createFrom("2021-02-01", "2021-02-01"),
                listOf(ColumnTypeDto("date", "DATE"), ColumnTypeDto("value", "INT"), ColumnTypeDto("type", "STRING"),
                        ColumnTypeDto("ids", "INT[]"))
        )

        val cachedResultPath: Path = Paths.get(ResourceUtils.getFile("classpath:cached-files/process-cache-request.csv").toURI())

        every { cloudDbConnectorProxy.query("SELECT event_type,date,ids,sum(value) FROM cache_test.cache_test_1 WHERE (date BETWEEN '2021-02-01' AND '2021-02-01') GROUP BY event_type,date,ids", "cache_test_1", 3) }
                .returns(Optional.of(TableResultDto(listOf("date", "value", "type"), listOf(cachedResultPath))))

        assertTrue(cacheService.processCacheRequest(resolvedQueryDto))

        val result = cacheService.getCachedResult(resolvedQueryDto)
        assertTrue(result.cached)
        assertEquals(2, result.data!!.rows.size)
        assertEquals("TEMP", result.data!!.rows[0][0])
        assertEquals(5, result.data!!.rows[0][1])
        assertEquals(java.sql.Date(2021 - 1900, 1, 1), result.data!!.rows[0][2])
        assertEquals(1, (result.data!!.rows[0][3] as Array<*>)[0])
        assertEquals(2, (result.data!!.rows[0][3] as Array<*>)[1])
    }

    @Test
    fun testCacheRequestWhenNoSpaceLeftShouldNotCache() {
        val resolvedQueryDto = ResolvedQueryDto(
                "select * from cache_test_1 where date='2020-01-01'",
                mapOf(),
                "cache_test_1",
                DatePartitionDto.createFrom("2020-01-01", "2020-01-01"),
                listOf(ColumnTypeDto("date", "DATE"), ColumnTypeDto("value", "INT"), ColumnTypeDto("type", "STRING"),
                        ColumnTypeDto("ids", "INT[]"))
        )

        every { cloudDbConnectorProxy.query("SELECT event_type,value,date,ids FROM cache_test.cache_test_1 WHERE (date BETWEEN '2020-01-01' AND '2020-01-01')", "cache_test_1", 3) }
                .returns(Optional.empty())

        assertFalse(cacheService.processCacheRequest(resolvedQueryDto))
    }




}