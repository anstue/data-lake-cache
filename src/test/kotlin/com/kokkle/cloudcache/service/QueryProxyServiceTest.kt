package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.dto.QueryDto
import com.kokkle.cloudcache.testconfig.IntegrationTestBigQueryConfiguration
import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@AutoConfigureEmbeddedDatabase(beanName = "dataSource")
@Import(IntegrationTestBigQueryConfiguration::class)
@PropertySource(value = ["classpath:application-test.properties"])
@Disabled("Unable to test google storage atm") //TODO fix
internal class QueryProxyServiceTest {

    @Autowired
    private lateinit var queryProxyService: QueryProxyService

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/integration-test-cached-results.sql"])
    fun testProxyQueryAllColumns_shouldBeCached() {
        val queryDto = QueryDto("select * from cache_test.cache_test_1 where date between '2020-01-01' and '2020-01-15'", mapOf())
        val result = queryProxyService.proxyQuery(queryDto)
        assertEquals(2, result.rows.size)
        assertEquals("2020-01-01", result.rows[0][1].toString())
        assertEquals("2020-01-15", result.rows[1][1].toString())
        assertEquals(1, (result.rows[1][3] as Array<*>)[0])
        assertEquals(2, (result.rows[1][3] as Array<*>)[1])
    }

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/integration-test-cached-results.sql"])
    fun testProxyQueryExtractOnlyOneColumn_shouldBeCached() {
        val queryDto = QueryDto("select date from cache_test.cache_test_1 where date between '2020-01-01' and '2020-01-15'", mapOf())
        val result = queryProxyService.proxyQuery(queryDto)
        assertEquals(2, result.rows.size)
        assertEquals("2020-01-01", result.rows[0][0].toString())
        assertEquals("2020-01-15", result.rows[1][0].toString())
    }

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/integration-test-cached-results.sql"])
    fun testGetSumOfValues_shouldBeCached() {
        val queryDto = QueryDto("select sum(value) from cache_test.cache_test_1 where date between '2020-01-01' and '2020-01-15'", mapOf())
        val result = queryProxyService.proxyQuery(queryDto)
        assertEquals(1, result.rows.size)
        assertEquals(3L, result.rows[0][0])
    }

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/integration-test-clean.sql"])
    fun testProxyQuery_shouldQueryBQAndCache() {
        //only expecting empty result, since free version of BQ removes old partitions automatically
        val queryDto = QueryDto("select * from cache_test.cache_test_1 where date between '2020-01-01' and '2020-04-01'", mapOf())
        val result = queryProxyService.proxyQuery(queryDto)
        assertEquals(0, result.rows.size)
    }
}