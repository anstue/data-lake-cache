package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.dao.CacheDao
import com.kokkle.cloudcache.dao.CacheMetadataDao
import com.kokkle.cloudcache.dao.postgres.PostgresCacheDao
import com.kokkle.cloudcache.dao.postgres.PostgresCacheMetadataDao
import com.kokkle.cloudcache.dto.ColumnTypeDto
import com.kokkle.cloudcache.dto.DatePartitionDto
import com.kokkle.cloudcache.dto.DateRangeDto
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.testconfig.JdbcTestConfiguration
import com.kokkle.cloudcache.testconfig.ServiceTestConfiguration
import com.kokkle.cloudcache.util.DateUtils
import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDateTime
import java.util.*

@ExtendWith(SpringExtension::class)
@AutoConfigureEmbeddedDatabase(beanName = "dataSource")
@Import(CacheInvalidatorService::class, ServiceTestConfiguration::class, JdbcTestConfiguration::class)
@PropertySource(value = ["classpath:application-test.properties"])
internal class CacheInvalidatorServiceTest {

    @Autowired
    private lateinit var cacheInvalidatorService: CacheInvalidatorService

    @Autowired
    private lateinit var cacheMetadataDao: CacheMetadataDao

    @Autowired
    private lateinit var cacheDao: CacheDao

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/test-cache-invalidator.sql"])
    fun testInvalidateCache_shouldRemoveAllCache() {
        val resolvedQueryDto = getResolvedQueryDto("cache_test_1")
        val resolvedQueryDto2 = getResolvedQueryDto("cache_test_2")

        cacheInvalidatorService.invalidateCache(0)

        assertEquals(0, cacheMetadataDao.getOldMetadata(LocalDateTime.now()).size)
        assertEquals(0, cacheDao.query(resolvedQueryDto).rows.size)
        assertEquals(0, cacheDao.query(resolvedQueryDto2).rows.size)
    }

    @Test
    @Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/service/test-cache-invalidator.sql"])
    fun testInvalidateCache_shouldRemoveOldCache() {
        val resolvedQueryDto = getResolvedQueryDto("cache_test_1")
        val resolvedQueryDto2 = getResolvedQueryDto("cache_test_2")

        cacheInvalidatorService.invalidateCache(1)
val metadata = cacheMetadataDao.getOldMetadata(LocalDateTime.now())
        assertEquals(2, metadata.size)
        assertEquals("2020-01-01", DateUtils.format(metadata[0].startDate))
        assertEquals("2020-02-01", DateUtils.format(metadata[0].endDate))
        assertEquals("2020-03-02", DateUtils.format(metadata[1].startDate))
        assertEquals("2020-04-01", DateUtils.format(metadata[1].endDate))

        val result = cacheDao.query(resolvedQueryDto).rows
        assertEquals(4, result.size)
        assertFalse(result.filter { it[0] == "T2" }.any())
        assertEquals(0, cacheDao.query(resolvedQueryDto2).rows.size)
    }

    private fun getResolvedQueryDto(tableName: String): ResolvedQueryDto {
        return ResolvedQueryDto(
                "select * from $tableName where date between '2020-01-01' and '2020-04-01'",
                mapOf(),
                tableName,
                DatePartitionDto(listOf(DateRangeDto("2020-01-01", "2020-04-01"))),
                listOf(ColumnTypeDto("date", "DATE"), ColumnTypeDto("value", "INT"), ColumnTypeDto("type", "STRING"),
                        ColumnTypeDto("ids", "INT[]"))
        )
    }
}