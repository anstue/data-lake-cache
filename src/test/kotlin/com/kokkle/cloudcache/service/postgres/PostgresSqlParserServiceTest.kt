package com.kokkle.cloudcache.service.postgres

import com.kokkle.cloudcache.dto.DatePartitionDto
import com.kokkle.cloudcache.dto.DateRangeDto
import com.kokkle.cloudcache.dto.QueryDto
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import com.kokkle.cloudcache.service.SqlParserService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@Import(PostgresSqlParserService::class)
@PropertySource(value = ["classpath:application-test.properties"])
internal class PostgresSqlParserServiceTest {

    @Autowired
    private lateinit var sqlParserService: SqlParserService

    @Test
    fun getPartitions() {
        val queryDto = QueryDto("select * from bla where date between '2020-01-01' and '2020-01-03'", mapOf())
        val datePartitions = sqlParserService.getPartitions(queryDto)
        assertEquals(1, datePartitions.ranges.size)
        assertEquals("2020-01-01", datePartitions.ranges[0].fromIncl)
        assertEquals("2020-01-03", datePartitions.ranges[0].untilIncl)
    }

    @Test
    fun getPartitionsCaseSensitivity() {
        val queryDto = QueryDto("select * from bla where date between '2020-01-01' AND '2020-01-03'", mapOf())
        val datePartitions = sqlParserService.getPartitions(queryDto)
        assertEquals(1, datePartitions.ranges.size)
        assertEquals("2020-01-01", datePartitions.ranges[0].fromIncl)
        assertEquals("2020-01-03", datePartitions.ranges[0].untilIncl)
    }

    @Test
    fun getPartitionsDynamically() {
        val thrown: NotImplementedError = assertThrows(
                NotImplementedError::class.java,
                {
                    //TODO in BQ - interval days is a bit different DATE_ADD(DATE "2008-12-25", INTERVAL 5 DAY)
                    val queryDto = QueryDto("select * from bla where date between NOW() - INTERVAL '7 DAY' AND NOW()", mapOf())
                    sqlParserService.getPartitions(queryDto)
                },
                "Should throw a not implemented error"
        )

        assertTrue(thrown.message!!.contains("Subtraction"))
    }

    @Test
    fun getPartitionsMultiplePartitionFilters() {
        val queryDto = QueryDto("select * from bla where date between '2020-01-15' AND '2020-01-31' and " +
                "id in (select id from bla where date between '2020-01-01' AND '2020-01-14' and 1=(2-1))", mapOf())
        val datePartitions = sqlParserService.getPartitions(queryDto)
        assertEquals(2, datePartitions.ranges.size)
        assertEquals("2020-01-15", datePartitions.ranges[0].fromIncl)
        assertEquals("2020-01-31", datePartitions.ranges[0].untilIncl)
        assertEquals("2020-01-01", datePartitions.ranges[1].fromIncl)
        assertEquals("2020-01-14", datePartitions.ranges[1].untilIncl)
    }

    @Test
    fun getPartitionsMultiplePartitionFiltersWithNot() {
        val queryDto = QueryDto("select * from bla where date between '2020-01-15' AND '2020-01-31' and " +
                " date <> '2020-01-14'", mapOf())
        val datePartitions = sqlParserService.getPartitions(queryDto)
        assertEquals(1, datePartitions.ranges.size)
        assertEquals("2020-01-15", datePartitions.ranges[0].fromIncl)
        assertEquals("2020-01-31", datePartitions.ranges[0].untilIncl)
    }

    @Test
    fun getPartitionsMultiplePartitionFiltersWithNot2() {
        val queryDto = QueryDto("select * from bla where date between '2020-01-15' AND '2020-01-31' and " +
                " not (date between '2020-01-01' AND '2020-01-14')", mapOf())
        val datePartitions = sqlParserService.getPartitions(queryDto)
        assertEquals(1, datePartitions.ranges.size)
        assertEquals("2020-01-15", datePartitions.ranges[0].fromIncl)
        assertEquals("2020-01-31", datePartitions.ranges[0].untilIncl)
    }

    @Test
    fun getPartitionsInFilter() {
        val queryDto = QueryDto("select * from bla where date in ('2020-01-15','2020-01-31') or date in ('2020-01-01', '2020-01-02')", mapOf())
        val datePartitions = sqlParserService.getPartitions(queryDto)
        assertEquals(4, datePartitions.ranges.size)
        assertEquals("2020-01-15", datePartitions.ranges[0].untilIncl)
        assertEquals("2020-01-15", datePartitions.ranges[0].fromIncl)
        assertEquals("2020-01-31", datePartitions.ranges[1].untilIncl)
        assertEquals("2020-01-31", datePartitions.ranges[1].fromIncl)
        assertEquals("2020-01-01", datePartitions.ranges[2].untilIncl)
        assertEquals("2020-01-01", datePartitions.ranges[2].fromIncl)
        assertEquals("2020-01-02", datePartitions.ranges[3].untilIncl)
        assertEquals("2020-01-02", datePartitions.ranges[3].fromIncl)
    }

    @Test
    fun getPartitionsBiggerAndSmaller() {
        val queryDto = QueryDto("select * from bla where date>'2020-01-15' and date<'2020-01-31' and value<>3", mapOf())
        val datePartitions = sqlParserService.getPartitions(queryDto)
        assertEquals(1, datePartitions.ranges.size)
        assertEquals("2020-01-15", datePartitions.ranges[0].fromIncl) // ignore one day of incorrectness
        assertEquals("2020-01-31", datePartitions.ranges[0].untilIncl)
    }

    @Test
    fun getPartitionsBiggerAndSmallerWith() {
        val queryDto = QueryDto("with c1 as (select * from bla where date>='2020-01-01' and date<='2020-01-14' and value<>3)," +
                "c2 as (select * from bla where date>='2020-01-15' and date<='2020-01-31' and value<>3)" +
                "select * from c1, c2", mapOf())
        val datePartitions = sqlParserService.getPartitions(queryDto)
        assertEquals(1, datePartitions.ranges.size)
        assertEquals("2020-01-01", datePartitions.ranges[0].fromIncl)
        assertEquals("2020-01-31", datePartitions.ranges[0].untilIncl)
    }

    @Test
    fun getPartitionsBiggerAndSmallerOrEqual() {
        val queryDto = QueryDto("select * from bla where date>='2020-01-15' and date<='2020-01-31' and value<>3", mapOf())
        val datePartitions = sqlParserService.getPartitions(queryDto)
        assertEquals(1, datePartitions.ranges.size)
        assertEquals("2020-01-15", datePartitions.ranges[0].fromIncl)
        assertEquals("2020-01-31", datePartitions.ranges[0].untilIncl)
    }

    @Test
    fun getOnePartition() {
        val queryDto = QueryDto("select * from bla where date='2020-01-31' and value=44", mapOf())
        val datePartitions = sqlParserService.getPartitions(queryDto)
        assertEquals(1, datePartitions.ranges.size)
        assertEquals("2020-01-31", datePartitions.ranges[0].fromIncl)
        assertEquals("2020-01-31", datePartitions.ranges[0].untilIncl)
    }

    @Test
    fun testGetTableName() {
        val queryDto = QueryDto("select * from cache_test.table_123 where date between '2020-01-01' AND '2020-01-03'", mapOf())
        val tableName = sqlParserService.getTableName(queryDto.query)
        assertEquals("table_123", tableName)
    }

    @Test
    fun testConvertQueryForCache() {
        val queryDto = ResolvedQueryDto("select * from cache_test.table_123 where date between '2020-01-01' AND '2020-01-03'", mapOf(),
                "table_123", DatePartitionDto(listOf(DateRangeDto("2020-01-01", "2020-01-03"))), listOf())
        val queryDtoRet = sqlParserService.convertQueryForCache(queryDto)
        assertEquals("select * from table_123 where date between '2020-01-01' AND '2020-01-03'", queryDtoRet.query)
    }

    @Test
    fun testGetCacheQuery() {
        val columns = setOf("dim1", "dim2", "units", "units2")
        val result = sqlParserService.getCacheQuery(columns, listOf("units", "units2"),
                ResolvedQueryDto("", mapOf(), "table1", DatePartitionDto(listOf(DateRangeDto("2020-01-01", "2020-01-01"))), listOf()))
        assertEquals("SELECT dim1,dim2,sum(units),sum(units2) FROM cache_test.table1 WHERE (date BETWEEN '2020-01-01' AND '2020-01-01') GROUP BY dim1,dim2", result)
    }
}