/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.service

import com.kokkle.cloudcache.dto.DatePartitionDto
import com.kokkle.cloudcache.dto.DateRangeDto
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import java.util.*
import java.util.concurrent.Executors

internal class CacheRequestQueueExecutorTest {

    private val executorService = Executors.newFixedThreadPool(4)
    private val tableCacheQueuer = mockk<TableCacheQueuer>()
    private val cacheService = mockk<CacheService>()

    private val cacheRequestQueueExecutor: CacheRequestQueueExecutor = CacheRequestQueueExecutor(executorService, tableCacheQueuer, cacheService)

    private val testObj1 = ResolvedQueryDto("test", mapOf(), "table1",
            DatePartitionDto(listOf(DateRangeDto("2020-01-01","2020-01-05"))), listOf()
    )

    private val testObj2 = ResolvedQueryDto("test", mapOf(), "table1",
            DatePartitionDto(listOf(DateRangeDto("2020-01-03","2020-01-06"))), listOf()
    )

    private val testObj3 = ResolvedQueryDto("test", mapOf(), "table2",
            DatePartitionDto(listOf(DateRangeDto("2020-01-03","2020-01-06"))), listOf()
    )


    @Test
    fun testProcess() {
        every { tableCacheQueuer.getNextItemFromQueue() } returns Optional.of(testObj1) andThen  Optional.of(testObj2) andThen  Optional.of(testObj3) andThen Optional.empty()
        cacheRequestQueueExecutor.init()
        Thread.sleep(2000)
        verify(exactly = 2) { cacheService.processCacheRequest(any()) }

    }
}