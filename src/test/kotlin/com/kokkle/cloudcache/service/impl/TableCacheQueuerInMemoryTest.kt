package com.kokkle.cloudcache.service.impl

import com.kokkle.cloudcache.dto.ColumnTypeDto
import com.kokkle.cloudcache.dto.DatePartitionDto
import com.kokkle.cloudcache.dto.ResolvedQueryDto
import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*

@ExtendWith(SpringExtension::class)
@AutoConfigureEmbeddedDatabase(beanName = "dataSource")
@Import(TableCacheQueuerInMemory::class)
@PropertySource(value = ["classpath:application-test.properties"])
internal class TableCacheQueuerInMemoryTest {

    @Autowired
    private lateinit var tableCacheQueuerInMemory: TableCacheQueuerInMemory

    @Test
    fun testInsertAndDequeue_shouldLockAndUnlock() {
        val resolvedQueryDto1 = ResolvedQueryDto(
                "select * from cache_test_1 where date='2020-01-01'",
                mapOf(),
                "cache_test_1",
                DatePartitionDto.createFrom("2020-01-01", "2020-01-01"),
                listOf(ColumnTypeDto("date", "DATE"), ColumnTypeDto("value", "INT"), ColumnTypeDto("type", "STRING"),
                        ColumnTypeDto("ids", "INT[]"))
        )
        val resolvedQueryDto2 = ResolvedQueryDto(
                "select * from cache_test_1 where date='2020-01-02'",
                mapOf(),
                "cache_test_1",
                DatePartitionDto.createFrom("2020-01-02", "2020-01-02"),
                listOf(ColumnTypeDto("date", "DATE"), ColumnTypeDto("value", "INT"), ColumnTypeDto("type", "STRING"),
                        ColumnTypeDto("ids", "INT[]"))
        )
        val resolvedQueryDto3 = ResolvedQueryDto(
                "select * from cache_test_2 where date='2020-01-01'",
                mapOf(),
                "cache_test_2",
                DatePartitionDto.createFrom("2020-01-01", "2020-01-01"),
                listOf(ColumnTypeDto("date", "DATE"), ColumnTypeDto("value", "INT"), ColumnTypeDto("type", "STRING"),
                        ColumnTypeDto("ids", "INT[]"))
        )

        tableCacheQueuerInMemory.queueCacheRequest(resolvedQueryDto1)
        tableCacheQueuerInMemory.queueCacheRequest(resolvedQueryDto2)
        tableCacheQueuerInMemory.queueCacheRequest(resolvedQueryDto3)

        tableCacheQueuerInMemory.getNextItemFromQueue().let {
            assertTrue(it.isPresent)
            assertEquals("cache_test_1", it.get().tableName)
        }

        tableCacheQueuerInMemory.getNextItemFromQueue().let {
            assertTrue(it.isPresent)
            assertEquals("cache_test_2", it.get().tableName)
        }

        //table1 locked, so no result should be returned
        assertFalse(tableCacheQueuerInMemory.getNextItemFromQueue().isPresent)

        tableCacheQueuerInMemory.unlockTable("cache_test_1")

        tableCacheQueuerInMemory.getNextItemFromQueue().let {
            assertTrue(it.isPresent)
            assertEquals("cache_test_1", it.get().tableName)
        }

    }

}