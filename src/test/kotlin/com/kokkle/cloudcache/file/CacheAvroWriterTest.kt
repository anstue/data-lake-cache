package com.kokkle.cloudcache.file

import com.kokkle.cloudcache.dto.RowDto
import com.kokkle.cloudcache.util.DateUtils
import com.kokkle.cloudcache.util.FileUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.nio.file.Files
import java.time.LocalDate

@ExtendWith(SpringExtension::class)
@Import(CacheAvroWriter::class)
@PropertySource(value = ["classpath:application-test.properties"])
internal class CacheAvroWriterTest {

    @Autowired
    private lateinit var cacheAvroWriter: CacheAvroWriter

    @Test
    fun testWriteToAvro() {
        val avroPath = FileUtils.getTmpFilePath("test_", ".avro")
        try {
            val today = "2020-01-02"
            val yesterday = "2020-01-01"
            val row1 = RowDto(listOf("TEMP", 1, today, listOf(1, 2)))
            val row2 = RowDto(listOf("TEMP", 5, yesterday, listOf(1, 2, 3)))
            val rows = listOf(row1, row2)
            cacheAvroWriter.writeToAvro(rows, org.apache.hadoop.fs.Path(avroPath.toUri()))
            val result = cacheAvroWriter.readFromAvro(org.apache.hadoop.fs.Path(avroPath.toUri()))
            assertEquals(4, result.columns.size)
            assertEquals(2, result.rows.size)
            assertEquals("TEMP", result.rows[0][0].toString())
            assertEquals(1, result.rows[0][1])
            assertEquals(DateUtils.parse(today), result.rows[0][2])
            assertEquals(listOf(1,2), result.rows[0][3])

            assertEquals("TEMP", result.rows[1][0].toString())
            assertEquals(5, result.rows[1][1])
            assertEquals(DateUtils.parse(yesterday), result.rows[1][2])
            assertEquals(listOf(1,2,3), result.rows[1][3])
        } finally {
            Files.deleteIfExists(avroPath)
        }
    }
}