package com.kokkle.cloudcache

import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureEmbeddedDatabase(beanName = "dataSource")
class CloudCacheApplicationTests {

	@Test
	fun contextLoads() {
	}

}
