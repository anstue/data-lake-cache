package com.kokkle.cloudcache.testconfig

import com.google.cloud.bigquery.BigQuery
import com.google.cloud.bigquery.BigQueryOptions
import com.kokkle.cloudcache.connector.BigQueryCloudConnector
import com.kokkle.cloudcache.connector.BigQueryCloudDbConnectorProxy
import com.kokkle.cloudcache.dao.postgres.PostgresCacheDao
import com.kokkle.cloudcache.dao.postgres.PostgresCacheMetadataDao
import com.kokkle.cloudcache.dao.postgres.PostgresInitializerDao
import com.kokkle.cloudcache.mapper.CacheMetadataMapper
import com.kokkle.cloudcache.mapper.TargetTableMapper
import com.kokkle.cloudcache.service.CacheMetadataService
import com.kokkle.cloudcache.service.CacheService
import com.kokkle.cloudcache.service.QueryProxyService
import com.kokkle.cloudcache.service.impl.TableCacheQueuerInMemory
import com.kokkle.cloudcache.service.postgres.PostgresSqlParserService
import io.micrometer.core.instrument.MeterRegistry
import io.mockk.every
import io.mockk.mockk
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import

@TestConfiguration
@Import(JdbcTestConfiguration::class, PostgresSqlParserService::class, PostgresInitializerDao::class, PostgresCacheDao::class,
        TargetTableMapper::class, CacheMetadataService::class, PostgresCacheMetadataDao::class,
        CacheMetadataMapper::class, BigQueryCloudDbConnectorProxy::class, BigQueryCloudConnector::class, QueryProxyService::class,
        CacheService::class, TableCacheQueuerInMemory::class)
class IntegrationTestBigQueryConfiguration {

    @Bean
    fun bigQuery(): BigQuery {
        return BigQueryOptions.getDefaultInstance().service
    }

    @Bean
    fun meterRegistry(): MeterRegistry {
        val mock = mockk<MeterRegistry>()
        every { mock.timer(any()) } returns mockk(relaxed = true)
        return mock
    }

}
