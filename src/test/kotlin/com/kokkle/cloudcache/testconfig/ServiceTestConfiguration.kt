package com.kokkle.cloudcache.testconfig

import com.kokkle.cloudcache.connector.BigQueryCloudDbConnectorProxy
import com.kokkle.cloudcache.connector.CloudDbConnectorProxy
import com.kokkle.cloudcache.dao.InitializerDao
import com.kokkle.cloudcache.dao.postgres.PostgresCacheDao
import com.kokkle.cloudcache.dao.postgres.PostgresCacheMetadataDao
import com.kokkle.cloudcache.dao.postgres.PostgresInitializerDao
import com.kokkle.cloudcache.mapper.CacheMetadataMapper
import com.kokkle.cloudcache.mapper.TargetTableMapper
import com.kokkle.cloudcache.service.CacheMetadataService
import com.kokkle.cloudcache.service.TableCacheQueuer
import com.kokkle.cloudcache.service.postgres.PostgresSqlParserService
import io.mockk.mockk
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import

@TestConfiguration
@Import(PostgresSqlParserService::class, PostgresInitializerDao::class, PostgresCacheDao::class,
        TargetTableMapper::class, CacheMetadataService::class, PostgresCacheMetadataDao::class,
        CacheMetadataMapper::class)
class ServiceTestConfiguration {

    @Bean
    fun cloudDbConnectorProxy(): CloudDbConnectorProxy {
        return mockk()
    }

    @Bean
    fun tableCacheQueuer(): TableCacheQueuer {
        return mockk()
    }

}