package com.kokkle.cloudcache.dao.postgres

import com.kokkle.cloudcache.dto.InsertDeleteCacheMetadataDto
import com.kokkle.cloudcache.dto.UpdateCacheMetadataDto
import com.kokkle.cloudcache.mapper.CacheMetadataMapper
import com.kokkle.cloudcache.testconfig.JdbcTestConfiguration
import com.kokkle.cloudcache.util.DateUtils
import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDateTime

@ExtendWith(SpringExtension::class)
@AutoConfigureEmbeddedDatabase(beanName = "dataSource")
@Import(PostgresCacheMetadataDao::class, JdbcTestConfiguration::class, CacheMetadataMapper::class)
@Sql(value = ["classpath:sql/init-db.sql", "classpath:sql/dao/test-cache-metadata.sql"])
internal class PostgresCacheMetadataDaoTest {

    @Autowired
    private lateinit var postgresCacheMetadataDao: PostgresCacheMetadataDao

    @Test
    fun testUpdateMetadata() {
        postgresCacheMetadataDao.updateMetadata(UpdateCacheMetadataDto(DateUtils.parse("2020-03-02"), DateUtils.parse("2020-04-01"), DateUtils.parse("2020-03-01"), DateUtils.parse("2020-03-02"), "cache_test_1",
                3, DateUtils.parse("2020-01-01").atStartOfDay()))

        val result = postgresCacheMetadataDao.getMetadataBetween("2020-03-02", "2020-03-02", "cache_test_1")

        assertEquals(1, result.size)
        assertEquals(DateUtils.parse("2020-03-01"), result.get(0).startDate)
        assertEquals(DateUtils.parse("2020-03-02"), result.get(0).endDate)
        assertEquals(3, result.get(0).hitCount)
        assertEquals(DateUtils.parse("2020-01-01").atStartOfDay(), result.get(0).lastHit)
    }

    @Test
    fun testInsertMetadata() {
        postgresCacheMetadataDao.insertMetadata(InsertDeleteCacheMetadataDto(DateUtils.parse("2019-01-01"), DateUtils.parse("2019-02-01"), "cache_test_2"))
        assertEquals(1, postgresCacheMetadataDao.getMetadataBetween("2019-01-01", "2019-02-01", "cache_test_2").size)

    }

    @Test
    fun testDeleteMetadata() {
        postgresCacheMetadataDao.deleteMetadata(
                listOf(InsertDeleteCacheMetadataDto(DateUtils.parse("2020-01-01"), DateUtils.parse("2020-02-01"), "cache_test_1")))
    }

    @Test
    fun testGetOldMetadata() {
        val oldMetadata = postgresCacheMetadataDao.getOldMetadata(LocalDateTime.now().plusHours(-6))
        assertEquals(1, oldMetadata.size)
        assertEquals("2020-02-02", DateUtils.format(oldMetadata.get(0).startDate))
        assertEquals("2020-03-01", DateUtils.format(oldMetadata.get(0).endDate))
    }

    @Test
    fun testGetMetadataBetween() {
        assertEquals(3, postgresCacheMetadataDao.getMetadataBetween("2020-01-01", "2020-04-02", "cache_test_1").size)
        assertEquals(1, postgresCacheMetadataDao.getMetadataBetween("2020-01-01", "2020-02-01", "cache_test_1").size)
        assertEquals(2, postgresCacheMetadataDao.getMetadataBetween("2020-01-01", "2020-02-02", "cache_test_1").size)
        assertEquals(1, postgresCacheMetadataDao.getMetadataBetween("2020-03-05", "2020-04-05", "cache_test_1").size)
        assertEquals(1, postgresCacheMetadataDao.getMetadataBetween("2020-03-05", "2020-03-06", "cache_test_1").size)
    }

    @Test
    fun testIsCached() {
        assertTrue(postgresCacheMetadataDao.isExactlyCached("2020-01-01", "2020-02-01", "cache_test_1"))
        assertFalse(postgresCacheMetadataDao.isExactlyCached("2021-01-01", "2021-01-02", "cache_test_1")) // partial cache at the beginning
        assertFalse(postgresCacheMetadataDao.isExactlyCached("2020-12-31", "2021-01-01", "cache_test_1")) // partial cache at the end
        assertFalse(postgresCacheMetadataDao.isExactlyCached("2020-12-31", "2021-01-02", "cache_test_1")) // partial cache in the middle
        // This is check in service layer
        // assertTrue(postgresCacheMetadataDao.isExactlyCached("2020-01-01", "2020-04-01", "cache_test_1")) // spawns over multiple rows
        assertTrue(postgresCacheMetadataDao.isExactlyCached("2020-01-01", "2020-01-05", "cache_test_1"))
        assertFalse(postgresCacheMetadataDao.isExactlyCached("2019-12-01", "2020-01-05", "cache_test_1"))
        assertTrue(postgresCacheMetadataDao.isExactlyCached("2020-04-01", "2020-04-01", "cache_test_1"))
        assertFalse(postgresCacheMetadataDao.isExactlyCached("2020-04-01", "2020-04-02", "cache_test_1"))
    }

    @Test
    fun testCacheHit() {
        postgresCacheMetadataDao.cacheHit("2020-01-01", "2020-02-01", "cache_test_1")

        val result = postgresCacheMetadataDao.getMetadataBetween("2020-01-01", "2020-02-01", "cache_test_1")

        assertEquals(1, result.size)
        assertEquals(1, result.get(0).hitCount)
        assertNotNull(result.get(0).lastHit)

    }
}