/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache.util

import com.kokkle.cloudcache.dto.DateRangeDto
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class SqlUtilsTest {

    @Test
    fun testGetOrConditionsForDateRanges() {
        val result = SqlUtils.getOrConditionsForDateRanges(listOf(DateRangeDto("2020-01-01", "2020-01-15"),
                DateRangeDto("2020-02-01", "2020-02-15")))
        assertEquals("(date BETWEEN '2020-01-01' AND '2020-01-15' OR date BETWEEN '2020-02-01' AND '2020-02-15')", result)
    }
}