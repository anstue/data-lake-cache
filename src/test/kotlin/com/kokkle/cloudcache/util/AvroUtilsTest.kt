package com.kokkle.cloudcache.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class AvroUtilsTest {

    @Test
    fun testCreateAvroSchemaFromPostgresSchema() {
        val testSchema = mapOf(
                "textone" to "text NOT NULL",
                "textone2" to "text",
                "value" to "INT NOT NULL",
                "date" to "DATE NOT NULL",
                "ids" to "int[] NOT NULL",
                "textArray" to "text[]",
                "longone" to "bigint")
        val schema = AvroUtils.createAvroSchemaFromPostgresSchema(testSchema)
        assertEquals(7, schema.fields.size)
        assertEquals("textone", schema.fields[0].name())
        assertEquals("string", schema.fields[0].schema().type.getName())
        assertEquals("textone2", schema.fields[1].name())
        assertEquals("null", schema.fields[1].schema().types[0].getName()) //can be string and null --> optional field
        assertEquals("string", schema.fields[1].schema().types[1].getName())
        assertEquals("value", schema.fields[2].name())
        assertEquals("int", schema.fields[2].schema().type.getName())
        assertEquals("date", schema.fields[3].name())
        assertEquals("int", schema.fields[3].schema().type.getName())
        assertEquals("date", schema.fields[3].schema().logicalType.getName())
        assertEquals("int", schema.fields[3].schema().type.getName())
        assertEquals("ids", schema.fields[4].name())
        assertEquals("array", schema.fields[4].schema().type.getName())
        assertEquals("\"int\"", schema.fields[4].schema().elementType.toString(false))
        assertEquals("textArray", schema.fields[5].name())
        assertEquals("array", schema.fields[5].schema().type.getName())
        assertEquals("\"string\"", schema.fields[5].schema().elementType.toString(false))
        assertEquals("longone", schema.fields[6].name())
        assertEquals("null", schema.fields[6].schema().types[0].getName())
        assertEquals("long", schema.fields[6].schema().types[1].getName())
    }

    @Test
    fun testConvertToAvroDate() {
        assertEquals(18659, AvroUtils.convertToAvroDate("2021-02-01"))
    }

    @Test
    fun testConvertIntArray() {
        assertEquals(listOf(1, 2, 3), AvroUtils.convertIntArray(listOf(1, 2, 3)))
    }

    @Test
    fun testConvertStringArray() {
        assertEquals(listOf("1", "2", "3"), AvroUtils.convertStringArray(listOf("1", "2", "3")))
    }
}