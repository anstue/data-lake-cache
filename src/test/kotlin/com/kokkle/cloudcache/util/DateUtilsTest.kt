package com.kokkle.cloudcache.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

internal class DateUtilsTest {

    @Test
    fun testConvertToLocalDate() {
        assertEquals("1970-01-01", DateUtils.format(DateUtils.convertToLocalDate(Date(0))))
    }

    @Test
    fun testConvertToLocalDateTime() {
        assertEquals("1970-01-01", DateUtils.format(DateUtils.convertToLocalDate(Date(0))))
    }

    @Test
    fun testParse() {
        val result = DateUtils.parse("2020-01-01")
        assertEquals("2020-01-01", DateUtils.format(result))
    }

}