/*
 * Copyright (c) 2018-2021.  anstue
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kokkle.cloudcache

import java.io.FileWriter
import kotlin.random.Random


fun main(args: Array<String>) {
    if (args.size != 1) {
        throw IllegalArgumentException("One argument is expected, the output file!")
    }
    FileWriter(args[0]).use { writer ->
        writer.write("{\n" +
                "  \"datasetName\": \"cache_test\",\n" +
                "  \"tableName\": \"cache_test_1\",\n" +
                "  \"rows\": [")
        for (i in 1..10_000_000) {
            writer.write("[\"TEMP\",${Random.nextInt(40)},\"2021-02-${String.format("%02d", Random.nextInt(27) + 1)}\"," +
                    "[${Random.nextInt(50)},${Random.nextInt(30)}]],\n")
        }
        writer.write("[\"TEMP\",5,\"2021-02-02\",[1,2]]\n")
        writer.write("  ]\n" +
                "}")
    }
}