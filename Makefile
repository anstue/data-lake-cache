VERSION ?= $(shell git tag -l '*.*.*' --sort=v:refname | tail -1)
MAJOR ?= $(shell test -n "git tag -l '*.*.*' --sort=v:refname" && git tag -l '*.*.*' --sort=v:refname | tail -1 | tr '.' ' ' | awk '{print $$1}' || echo "0")
MINOR ?= $(shell test -n "git tag -l '*.*.*' --sort=v:refname" && git tag -l '*.*.*' --sort=v:refname | tail -1 | tr '.' ' ' | awk '{print $$2}' || echo "0")
PATCH ?= $(shell test -n "git tag -l '*.*.*' --sort=v:refname" && git tag -l '*.*.*' --sort=v:refname | tail -1 | tr '.' ' ' | awk '{print $$3}' || echo "0")

major:
	@printf "$$(( $(MAJOR) + 1 )).0.0"

minor:
	@printf "$(MAJOR).$$(( $(MINOR) + 1 )).0"

patch:
	@printf "$(MAJOR).$(MINOR).$$(( $(PATCH) + 1 ))"