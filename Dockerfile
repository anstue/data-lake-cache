FROM openjdk:15-alpine

RUN apk --no-cache add tzdata
ENV TZ="Europe/Amsterdam"

VOLUME /tmp
ADD build/libs/*.jar /app.jar

RUN sh -c 'touch /app.jar'
ENV JAVA_OPTIONS="-server -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/var/heap-dump"

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTIONS $INSTANCE_JAVA_OPTIONS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]